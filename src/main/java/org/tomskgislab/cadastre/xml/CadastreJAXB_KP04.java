package org.tomskgislab.cadastre.xml;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.tomskgislab.landprocessor.shema.share.CadastreDictionaryLoader;
import org.tomskgislab.landprocessor.shema.stdkp4.*;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Кадастровый паспорт V04
 * @author Филиппов Владислав
 * @since 4.2
 */
public class CadastreJAXB_KP04 {
    private static Logger logger = LogManager.getLogger(CadastreJAXB_KP.class);
    // Кадастровый паспорт
    // Весь документ
    private RegionCadastrVidimusKP kp;
    // Участок
    TParcel parcel;
    //
    private CadastreDictionaryLoader dicts;
    //private TCadastralBlock quartal;
    private SimpleFeatureBuilder parcelBuilder;
    private DefaultFeatureCollection parcelCollection;

    public CadastreJAXB_KP04(File Path, DefaultFeatureCollection ParcelCollection,
                           SimpleFeatureBuilder ParcelBuilder, CadastreDictionaryLoader Dicts) {
        try {
            this.dicts = Dicts;
            this.parcelBuilder= ParcelBuilder;
            this.parcelCollection = ParcelCollection;
            JAXBContext context = JAXBContext.newInstance(RegionCadastrVidimusKP.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(Path);
            kp = (RegionCadastrVidimusKP)obj;
            if (kp != null) {
                parcel = kp.getPackage().getParcels().getParcel().get(0);
                parse();
                parseParcelPart();

            }
        } catch (JAXBException ex) {
            kp = null;
            logger.error(ex.getLocalizedMessage());
        }
    }

    private void parse() {
        try{
            if (parcel !=null) {
                MultiPolygon parcelGeometry = null;

                if (getParcelType(parcel).equalsIgnoreCase("02")) {
                    logger.info("Обрабатывается  единое землепользование");
                }

                // Геометрия ЗУ
                if(parcel.getContours() == null) {
                    if (parcel.getEntitySpatial() == null) {
                        logger.info("У ЗУ " + this.getCadastreNumber(parcel) + " нет тега EntitySpatial");
                        return;
                    }
                    else {
                        CadastreEntitySpatialKP04 ces = new CadastreEntitySpatialKP04(parcel.getEntitySpatial());
                        Polygon p = ces.getPolygon();
                        if (p == null) {
                            logger.warn("Геометри контура ЗУ " + this.getCadastreNumber(parcel) + " пустая!");
                            //return;
                        }
                        Polygon[] pa = new Polygon[1];
                        pa[0] = p;
                        parcelGeometry = new MultiPolygon(pa, ces.getGeometryFactory());
                    }
                }
                else if (parcel.getContours().getContour().size() > 0) {
                    Polygon[] pa = new Polygon[parcel.getContours().getContour().size()];
                    for(int i=0; i<parcel.getContours().getContour().size(); i++) {
                        CadastreEntitySpatialKP04 ces = new CadastreEntitySpatialKP04(parcel.getContours().getContour().get(i).getEntitySpatial());
                        Polygon p = ces.getPolygon();
                        pa[i] = p;
                    }
                    parcelGeometry = new MultiPolygon(pa, JTSFactoryFinder.getGeometryFactory(null));
                }


                if ((parcelGeometry != null) && (parcelGeometry.isValid())) {
                    this.parcelBuilder.add(parcelGeometry);
                    this.parcelBuilder.add("ParcelsFromKV");
                    this.parcelBuilder.add(getCadastreNumber(parcel));
                    this.parcelBuilder.add(getUtilizationByDoc(parcel));
                    this.parcelBuilder.add(getUtilizationByType(parcel));
                    this.parcelBuilder.add(getParcelType(parcel));
                    this.parcelBuilder.add(getParcelState(parcel));

                    // // Могут быть 2 права (Собственность у РФ и ПБП у
                    // какой-нибудь госконторы)
                    this.parcelBuilder.add(getParcelRigth1(parcel));
                    this.parcelBuilder.add(getParcelRigth2(parcel));

                    this.parcelBuilder.add(this.getCategory(parcel));

                    // в квадратных метрах
                    this.parcelBuilder.add(getParcelArea(parcel));
                    this.parcelBuilder.add(getParcelAreaUnit(parcel));

                    // Правообладатели
                    this.parcelBuilder.add(getParcelRigthOwner1(parcel));
                    this.parcelBuilder.add(getParcelRigthOwner2(parcel));

                    this.parcelBuilder.add(LandProcessorConstants.NODATA);
                    this.parcelBuilder.add(LandProcessorConstants.NODATA);
                    //Encumbrance
                    this.parcelBuilder.add(LandProcessorConstants.NODATA);
                    this.parcelBuilder.add(LandProcessorConstants.NODATA);
                    this.parcelBuilder.add(LandProcessorConstants.NODATA);

                    this.parcelBuilder.add(getParcelRegion(parcel));
                    this.parcelBuilder.add(getParcelParish(parcel));
                    this.parcelBuilder.add(getParcelCity(parcel));
                    this.parcelBuilder.add(getParcelDistrict(parcel));
                    this.parcelBuilder.add(getParcelVillage(parcel));
                    this.parcelBuilder.add(getParcelLocality(parcel));
                    this.parcelBuilder.add(getParcelStreet(parcel));
                    this.parcelBuilder.add(getParcelLevel1(parcel));
                    this.parcelBuilder.add(getParcelLevel2(parcel));
                    this.parcelBuilder.add(getParcelLevel3(parcel));
                    this.parcelBuilder.add(getParcelFlat(parcel));

                    this.parcelBuilder.add(getParcelOther(parcel));
                    this.parcelBuilder.add(getParcelNote(parcel));
                    this.parcelBuilder.add(getParcelNote2(parcel));

                    SimpleFeature feature = this.parcelBuilder
                            .buildFeature(null);
                    this.parcelCollection.add(feature);

                }
            }
            logger.info("Конец обработки ЗУ  "
                    + this.getCadastreNumber(parcel));

        }
        catch (Exception ex) {
            logger.error(ex.getLocalizedMessage());
        }
    }

    private void parseParcelPart() {
        try {
            // Геометрия чЗУ

            if (parcel.getSubParcels() != null) {
                logger.info("Обработка частей ЗУ");
                for(int i=0; i<parcel.getSubParcels().getSubParcel().size(); i++){
                    if (parcel.getSubParcels().getSubParcel().get(i).getEntitySpatial() != null) {
                        CadastreEntitySpatialKP04 ces = new
                                CadastreEntitySpatialKP04(parcel.getSubParcels().getSubParcel().get(i).getEntitySpatial());
                        Polygon parcelPartGeometry = ces.getPolygon();
                        if (parcelPartGeometry == null) {
                            logger.warn("Геометрия контура чЗУ " +
                                    this.getCadastreNumberPart(parcel.getSubParcels().getSubParcel().get(i), parcel) + " пустая!");
                            return;
                        }

                        TSubParcel pp = parcel.getSubParcels().getSubParcel().get(i);
                        this.parcelBuilder.add(parcelPartGeometry);
                        this.parcelBuilder.add("ParcelsPartFromKV");
                        this.parcelBuilder.add(getCadastreNumberPart(pp, parcel));
                        this.parcelBuilder.add(getUtilizationByDoc(parcel));
                        this.parcelBuilder.add(getUtilizationByType(parcel));
                        this.parcelBuilder.add(getParcelPartEntryObjType(pp));
                        this.parcelBuilder.add(getParcelState(parcel));

                        // Могут быть 2 права (Собственность у РФ и ПБП у
                        // какой-нибудь госконторы)
                        this.parcelBuilder.add(getParcelRigth1(parcel));
                        this.parcelBuilder.add(getParcelRigth2(parcel));

                        this.parcelBuilder.add(this.getCategory(parcel));

                        // в квадратных метрах
                        this.parcelBuilder.add(getParcelPartArea(pp));
                        this.parcelBuilder.add(getParcelPartAreaUnit(pp));

                        // Правообладатели
                        this.parcelBuilder.add(getParcelRigthOwner1(parcel));
                        this.parcelBuilder.add(getParcelRigthOwner2(parcel));

                        //
                        this.parcelBuilder.add(getParcelPartCadNumber(pp));
                        this.parcelBuilder.add(getParcelPartEntryObjType(pp));
                        //Encumbrance
                        this.parcelBuilder.add(getParcelPartEncName(pp));
                        this.parcelBuilder.add(getParcelPartEncType(pp));
                        this.parcelBuilder.add(getParcelPartEncOwner(pp));

                        this.parcelBuilder.add(getParcelRegion(parcel));
                        this.parcelBuilder.add(getParcelParish(parcel));
                        this.parcelBuilder.add(getParcelCity(parcel));
                        this.parcelBuilder.add(getParcelDistrict(parcel));
                        this.parcelBuilder.add(getParcelVillage(parcel));
                        this.parcelBuilder.add(getParcelLocality(parcel));
                        this.parcelBuilder.add(getParcelStreet(parcel));
                        this.parcelBuilder.add(getParcelLevel1(parcel));
                        this.parcelBuilder.add(getParcelLevel2(parcel));
                        this.parcelBuilder.add(getParcelLevel3(parcel));
                        this.parcelBuilder.add(getParcelFlat(parcel));

                        this.parcelBuilder.add(getParcelOther(parcel));
                        this.parcelBuilder.add(getParcelNote(parcel));
                        this.parcelBuilder.add(getParcelNote2(parcel));

                        SimpleFeature feature = this.parcelBuilder
                                .buildFeature(null);
                        this.parcelCollection.add(feature);
                    }
                }
            }
        }
        catch (Exception ex) {
            logger.error(ex.getLocalizedMessage());
        }
    }

    private String getCategory(TParcel parcel) {
        String Key = parcel.getCategory().getCategory();
        return dicts.getValueFromDictionary(LandProcessorConstants.dCategories,
                Key);
    }

    private String getCadastreNumber(TParcel parcel) {
        try {
//            if (parcel.getCadastralNumber().indexOf(":") == 0)
//                return quartal.getCadastralNumber() + parcel.getCadastralNumber();
            if (parcel.getCadastralNumber().indexOf(":") == 2)
                return parcel.getCadastralNumber();
            return LandProcessorConstants.NODATA;
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getCadastreNumberPart(TSubParcel Part, TParcel parcel) {
        try {
//            if(Part.getEncumbrance() != null) {
//                return Part.getEncumbrance().getName();
//            }
//            else if (Part.getObjectEntry() != null) {
//                return Part.getObjectEntry().getCadastralNumber();
//            }
//            else {
//                return LandProcessorConstants.NODATA;
//            }
            return getCadastreNumber(parcel) + "/" + Part.getNumberRecord();
            // TODO Разобраться
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getUtilizationByDoc(TParcel parcel) {
        try {
            return parcel.getUtilization().getByDoc();
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getUtilizationByType(TParcel parcel) {
        // Тут нужно значение по типу из справочника
        try {
            String kind = parcel.getUtilization().getKind();
            return dicts.getValueFromDictionary(
                    LandProcessorConstants.dUtilizations, kind);
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelType(TParcel parcel) {
        // Тут нужно значение по типу из справочника dParcels.xsd
        try {
            switch (parcel.getName()) {
                case "01":
                    return LandProcessorConstants.PARCEL_TYPE_01;
                case "02":
                    return LandProcessorConstants.PARCEL_TYPE_02;
                case "03":
                    return LandProcessorConstants.PARCEL_TYPE_03;
                case "04":
                    return LandProcessorConstants.PARCEL_TYPE_04;
                case "05":
                    return LandProcessorConstants.PARCEL_TYPE_05;
                case "06":
                    return LandProcessorConstants.PARCEL_TYPE_06;
                default:
                    return LandProcessorConstants.NODATA;
            }
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelState(TParcel parcel) {
        // Тут нужно значение по типу из справочника
        try {
            String stateKey = parcel.getState();
            return dicts.getValueFromDictionary(LandProcessorConstants.dStates,
                    stateKey);
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    // иногда информации по правам нет
    private List<TRight> getRightsNode(TParcel parcel) {
        if ((parcel.getRights() != null) &&
                (parcel.getRights().getRight() != null)) {
            return parcel.getRights().getRight();
        }
        else {
            logger.warn("Нет информации о правах");
            return null;
        }
    }

    private String getParcelRigth1(TParcel parcel) {
        try {
            if (getRightsNode(parcel) != null) {
                String RigthKey = getRightsNode(parcel).get(0).getType();
                return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
                        RigthKey);
            }
            else {
                return LandProcessorConstants.NODATA;
            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelRigth2(TParcel parcel) {
        try {
            if((getRightsNode(parcel) != null) && (getRightsNode(parcel).size() > 1)) {
                String RigthKey = getRightsNode(parcel).get(1).getType();
                return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
                        RigthKey);
            }
            else {
                return LandProcessorConstants.NODATA;
            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelArea(TParcel parcel) {
        try {
            return parcel.getArea().getArea().toString();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelAreaUnit(TParcel parcel) {
        try {
            String AreaUnitKey = parcel.getArea().getUnit();
            return dicts.getValueFromDictionary(LandProcessorConstants.dUnit,
                    AreaUnitKey);
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelRigthOwner1(TParcel parcel) {
        try {
            if (parcel.getRights().getRight().get(0) != null) {
                return getOwner(parcel.getRights().getRight().get(0), 0);
            }
            else
                return LandProcessorConstants.NODATA;
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }
    // Обрабатываю только 2 права. Вариант для шейп-файла.
    private String getParcelRigthOwner2(TParcel parcel) {
        try {
            if (parcel.getRights().getRight().get(1) != null) {
                return getOwner(parcel.getRights().getRight().get(1), 0);
            }
            else
                return LandProcessorConstants.NODATA;
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getOwner(TRight Right, int i) {
        try {
            if (Right.getOwner().get(i).getGovernance() != null){
                return Right.getOwner().get(i).getGovernance().getName();
            }
            else if (Right.getOwner().get(i).getOrganization() != null){
                return Right.getOwner().get(i).getOrganization().getName();
            }
            // Person
            else {
                String fio = Right.getOwner().get(i).getPerson().getFIO().getSurname() + " ";
                fio += Right.getOwner().get(i).getPerson().getFIO().getFirst() + " ";
                if (Right.getOwner().get(i).getPerson().getFIO().getPatronymic() != null) {
                    return fio += Right.getOwner().get(i).getPerson().getFIO().getPatronymic();
                }
                else
                    return fio;
            }
        }
        catch(Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelRegion(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress().getOther() == null)
                return LandProcessorConstants.NODATA;
            final String regionKey = parcel.getLocation().getAddress().getRegion();
            return dicts.getValueFromDictionary(LandProcessorConstants.dRegions, regionKey);
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelParish(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.District d = parcel.getLocation().getAddress().getDistrict();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelCity(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.City d = parcel.getLocation().getAddress().getCity();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelDistrict(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.UrbanDistrict d = parcel.getLocation().getAddress().getUrbanDistrict();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelVillage(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.SovietVillage d = parcel.getLocation().getAddress().getSovietVillage();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType().value();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLocality(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Locality d = parcel.getLocation().getAddress().getLocality();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelStreet(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Street d = parcel.getLocation().getAddress().getStreet();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLevel1(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Level1 d = parcel.getLocation().getAddress().getLevel1();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLevel2(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Level2 d = parcel.getLocation().getAddress().getLevel2();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLevel3(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Level3 d = parcel.getLocation().getAddress().getLevel3();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelFlat(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Apartment d = parcel.getLocation().getAddress().getApartment();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelOther(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress().getOther() == null)
                return LandProcessorConstants.NODATA;
            return parcel.getLocation().getAddress().getOther();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelNote(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress().getNote() == null)
                return LandProcessorConstants.NODATA;
            return parcel.getLocation().getAddress().getNote();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelNote2(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getElaboration() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getElaboration().getReferenceMark() == null)
                return LandProcessorConstants.NODATA;
            return parcel.getLocation().getElaboration().getReferenceMark();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

//    public RegionCadastrVidimusKV getKv() {
//        return kv;
//    }

    private String getParcelPartArea(TSubParcel parcelPart) {
        try {
            return parcelPart.getArea().getArea().toString();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelPartAreaUnit(TSubParcel parcelPart) {
        try {
            String AreaUnitKey = parcelPart.getArea().getUnit();
            return dicts.getValueFromDictionary(LandProcessorConstants.dUnit,
                    AreaUnitKey);
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelPartEntryObjType(TSubParcel parcelPart) {
        try {
            if (parcelPart.getObjectEntry() != null) {
                String RealtyKey = parcelPart.getObjectEntry().getType();
                return dicts.getValueFromDictionary(LandProcessorConstants.dRealty,
                        RealtyKey);
            }
            else {
                return LandProcessorConstants.NODATA;
            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelPartCadNumber(TSubParcel parcelPart) {
        try {
            if (parcelPart.getObjectEntry() != null) {
                return parcelPart.getObjectEntry().getCadastralNumber();
            }
            else {
                return LandProcessorConstants.NODATA;
            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelPartEncType(TSubParcel parcelPart) {
        try {
            if (parcelPart.getEncumbrance() != null) {
                String TypeyKey = parcelPart.getEncumbrance().getType();
                return dicts.getValueFromDictionary(LandProcessorConstants.dEncumbrances,
                        TypeyKey);
            }
            else {
                return LandProcessorConstants.NODATA;
            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelPartEncName(TSubParcel parcelPart) {
        try {
            if (parcelPart.getEncumbrance() != null) {
                return parcelPart.getEncumbrance().getName();
            }
            else {
                return LandProcessorConstants.NODATA;
            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getOwner2(TRightOwner Right) {
        try {
//            if (Right.getGovernance() != null){
//                return Right.getGovernance().getName();
//            }
//            else if (Right.getOrganization() != null){
//                return Right.getOrganization().getName();
//            }
//            // Person
//            else {
//                String fio = Right.getPerson().getFIO().getSurname() + " ";
//                fio += Right.getPerson().getFIO().getFirst() + " ";
//                if (Right.getPerson().getFIO().getPatronymic() != null) {
//                    return fio += Right.getPerson().getFIO().getPatronymic();
//                }
//                else
//                    return fio;
//            }
            return LandProcessorConstants.NODATA;
        }
        catch(Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelPartEncOwner(TSubParcel parcelPart) {
        try {
            // TODO Разобраться
//            if ((parcelPart.getEncumbrance() != null) && (parcelPart.getEncumbrance() != null)) {
//                return getOwner2(parcelPart.getEncumbrance().getOwnerRestrictionInFavorem()
//                        .get(0));
//            }
//            else {
                return LandProcessorConstants.NODATA;
//            }
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }
}
