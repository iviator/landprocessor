package org.tomskgislab.cadastre.xml;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.tomskgislab.landprocessor.shema.share.CadastreDictionaryLoader;
import org.tomskgislab.landprocessor.shema.stdkpt.*;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Кадастровый план территории
 * @author Филиппов Владислав
 * @since 1.2
 */
public class CadastreJAXB_KPT07 {

	private static Logger logger = LogManager.getLogger(CadastreJAXB_KPT07.class);

	// Участок
	private Parcels parcels;
	private TCadastralBlock quartal;
	//
	private CadastreDictionaryLoader dicts;
	private MultiPolygon parcelGeometry;
	// Части участка
	private SimpleFeatureBuilder parcelBuilder;
	private DefaultFeatureCollection parcelCollection;

	public CadastreJAXB_KPT07(File Path,
			DefaultFeatureCollection ParcelCollection,
			SimpleFeatureBuilder ParcelBuilder, CadastreDictionaryLoader Dicts) {
		RegionCadastr kpt;
		try {
			this.dicts = Dicts;
			this.parcelBuilder = ParcelBuilder;
			this.parcelCollection = ParcelCollection;
			JAXBContext context = JAXBContext.newInstance(RegionCadastr.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Object obj = unmarshaller.unmarshal(Path);
			kpt = (RegionCadastr) obj;
			if (kpt != null) {
				parcels = kpt.getPackage().getFederal().getCadastralRegions()
						.getCadastralRegion().get(0).getCadastralDistricts()
						.getCadastralDistrict().get(0).getCadastralBlocks()
						.getCadastralBlock().get(0).getParcels();
				quartal = kpt.getPackage().getFederal().getCadastralRegions()
						.getCadastralRegion().get(0).getCadastralDistricts()
						.getCadastralDistrict().get(0).getCadastralBlocks()
						.getCadastralBlock().get(0);
				
				parse();
				// Походу нет частей в КПТ
				// parseSubParcel();
			}
		} catch (JAXBException ex) {
			logger.error(ex.getLocalizedMessage());
		}
	}

	private void parse() {
		try {
			for (int j = 0; j < parcels.getParcel().size(); j++) {
				TParcel parcel = parcels.getParcel().get(j);
				logger.info("Начало обработки ЗУ "
						+ this.getCadastreNumber(parcel));
				if (parcel != null) {
					try {
					// Одноконтурный
					if (parcel.getContours() == null) {
						CadastreEntitySpatialKPT ces = new CadastreEntitySpatialKPT(
								parcel.getEntitySpatial());
						Polygon p = ces.getPolygon();
						if (p == null) {
							logger.warn("Геометри контура ЗУ "
									+ this.getCadastreNumber(parcel)
									+ " пустая!");
							continue;
						}
						Polygon[] pa = new Polygon[1];
						pa[0] = p;
						parcelGeometry = new MultiPolygon(pa,
								GeometryBuilder.getGeometryFactory());
					}
					// Многоконтурный
					else if (parcel.getContours().getContour().size() > 0) {
						
						Polygon[] pa = new Polygon[parcel.getContours()
								.getContour().size()];
						for (int i = 0; i < parcel.getContours().getContour()
								.size(); i++) {
							CadastreEntitySpatialKPT ces = new CadastreEntitySpatialKPT(
									parcel.getContours().getContour().get(i)
											.getEntitySpatial());
							Polygon p = ces.getPolygon();
							pa[i] = p;
							int k=i+1;
							logger.info("Многоконтурная геометрия. Контур " + k);
						}
						parcelGeometry = new MultiPolygon(pa,
								JTSFactoryFinder.getGeometryFactory(null));
						}
					}
					catch(Exception ex) {
						logger.error(ex);
						return;
					}

					// Добавлять значения в поля в том порядке, в каком
					// добавлены поля
					// в целевом шейпе.
//					if (parcelGeometry.isValid()) {
						this.parcelBuilder.add(parcelGeometry);
						this.parcelBuilder.add("ParcelsFromKPT");
						this.parcelBuilder.add(getCadastreNumber(parcel));
						this.parcelBuilder.add(getUtilizationByDoc(parcel));
						this.parcelBuilder.add(getUtilizationByType(parcel));
						this.parcelBuilder.add(getParcelType(parcel));
						this.parcelBuilder.add(getParcelState(parcel));

						// Могут быть 2 права (Собственность у РФ и ПБП у
						// какой-нибудь госконторы)
						this.parcelBuilder.add(getParcelRigth1(parcel));
						this.parcelBuilder.add(getParcelRigth2(parcel));

						this.parcelBuilder.add(this.getCategory(parcel));
						
						// в квадратных метрах
						this.parcelBuilder.add(getParcelArea(parcel));
						this.parcelBuilder.add(getParcelAreaUnit(parcel));

						this.parcelBuilder.add(LandProcessorConstants.NODATA);
						this.parcelBuilder.add(LandProcessorConstants.NODATA);
						this.parcelBuilder.add(LandProcessorConstants.NODATA);
						this.parcelBuilder.add(LandProcessorConstants.NODATA);
						this.parcelBuilder.add(LandProcessorConstants.NODATA);
						this.parcelBuilder.add(LandProcessorConstants.NODATA);
						this.parcelBuilder.add(LandProcessorConstants.NODATA);

						this.parcelBuilder.add(getParcelRegion(parcel));
						this.parcelBuilder.add(getParcelParish(parcel));
						this.parcelBuilder.add(getParcelCity(parcel));
						this.parcelBuilder.add(getParcelDistrict(parcel));
						this.parcelBuilder.add(getParcelVillage(parcel));
						this.parcelBuilder.add(getParcelLocality(parcel));
						this.parcelBuilder.add(getParcelStreet(parcel));
						this.parcelBuilder.add(getParcelLevel1(parcel));
						this.parcelBuilder.add(getParcelLevel2(parcel));
						this.parcelBuilder.add(getParcelLevel3(parcel));
						this.parcelBuilder.add(getParcelFlat(parcel));

						this.parcelBuilder.add(getParcelOther(parcel));
						this.parcelBuilder.add(getParcelNote(parcel));
						this.parcelBuilder.add(getParcelNote2(parcel));

						SimpleFeature feature = this.parcelBuilder
								.buildFeature(null);
						this.parcelCollection.add(feature);
//					} else {
//						logger.info("Геометрия не прошла проверку  "
//								+ this.getCadastreNumber(parcel));
//					}
				}
				logger.info("Конец обработки ЗУ  "
						+ this.getCadastreNumber(parcel));
			}
		} catch (Exception ex) {
			logger.error(ex.getLocalizedMessage());
		}
	}

	private String getCategory(TParcel parcel) {
		String Key = parcel.getCategory().getCategory();
		return dicts.getValueFromDictionary(LandProcessorConstants.dCategories,
				Key);
	}

	private String getCadastreNumber(TParcel parcel) {
		try {
			if (parcel.getCadastralNumber().indexOf(":") == 0) {
				return quartal.getCadastralNumber() + parcel.getCadastralNumber();
			}
			else if (parcel.getCadastralNumber().indexOf(":") == 2) {
				return parcel.getCadastralNumber();
			}
			else {
				return LandProcessorConstants.NODATA;
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getCadastreNumberPart(TSubParcel Part, TParcel parcel) {
		try {
			return getCadastreNumber(parcel) + "/" + Part.getNumberPP();
		}
		catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getUtilizationByDoc(TParcel parcel) {
		try {
			return parcel.getUtilization().get(0).getByDoc();
		}
		catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getUtilizationByType(TParcel parcel) {
		// Тут нужно значение по типу из справочника
		try {
			String kind = parcel.getUtilization().get(0).getKind();
			return dicts.getValueFromDictionary(
					LandProcessorConstants.dUtilizations, kind);
		}
		catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelType(TParcel parcel) {
		// Тут нужно значение по типу из справочника dParcels.xsd
		try {
			switch (parcel.getName()) {
			case "01":
				return LandProcessorConstants.PARCEL_TYPE_01;
			case "02":
				return LandProcessorConstants.PARCEL_TYPE_02;
			case "03":
				return LandProcessorConstants.PARCEL_TYPE_03;
			case "04":
				return LandProcessorConstants.PARCEL_TYPE_04;
			case "05":
				return LandProcessorConstants.PARCEL_TYPE_05;
			case "06":
				return LandProcessorConstants.PARCEL_TYPE_06;	
			default:
				return LandProcessorConstants.NODATA;
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelState(TParcel parcel) {
		// Тут нужно значение по типу из справочника
		try {
			String stateKey = parcel.getState();
			return dicts.getValueFromDictionary(LandProcessorConstants.dStates,
					stateKey);
		}
		catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}
	
	// иногда информации по правам нет
	private List<TRight> getRightsNode(TParcel parcel) {
		if ((parcel.getRights() != null) && 
				(parcel.getRights().getRight() != null)) {
			return parcel.getRights().getRight();
		}
		else {
			logger.warn("Нет информации о правах");
			return null;
		}
	}
	
	private String getParcelRigth1(TParcel parcel) {
		try {
			if (getRightsNode(parcel) != null) { 
				String RigthKey = getRightsNode(parcel).get(0).getType();
				return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
						RigthKey);
			}
			else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelRigth2(TParcel parcel) {
		try {
			if((getRightsNode(parcel) != null) && (getRightsNode(parcel).size() > 1)) {
				String RigthKey = getRightsNode(parcel).get(1).getType();
				return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
						RigthKey);
			}
			else {
				return LandProcessorConstants.NODATA;
			}
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}
	
	private String getParcelArea(TParcel parcel) {
		try {
			return parcel.getAreas().getArea().get(0).getArea().toString();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}
	
	private String getParcelAreaUnit(TParcel parcel) {
		try {
			String AreaUnitKey = parcel.getAreas().getArea().get(0).getUnit();
			return dicts.getValueFromDictionary(LandProcessorConstants.dUnit,
					AreaUnitKey);
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}
	
//	private String getParcelRigthOwner1(TParcel parcel) {
//		try {
//			String RigthKey = parcel.getRights().getRight().get(1).getType();
//			return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
//					RigthKey);
//		} catch (Exception ex) {
//			logger.error(ex);
//			return "--";
//		}
//	}
//	
//	private String getParcelRigthOwner2(TParcel parcel) {
//		try {
//			String RigthKey = parcel.getRights().getRight().get(1).;
//			return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
//					RigthKey);
//		} catch (Exception ex) {
//			logger.error(ex);
//			return "--";
//		}
//	}
	private String getParcelRegion(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress().getOther() == null)
				return LandProcessorConstants.NODATA;
			final String regionKey = parcel.getLocation().getAddress().getRegion();
			return dicts.getValueFromDictionary(LandProcessorConstants.dRegions, regionKey);
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelParish(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.District d = parcel.getLocation().getAddress().getDistrict();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getName() + " " + d.getType();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelCity(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.City d = parcel.getLocation().getAddress().getCity();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getName() + " " + d.getType();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelDistrict(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.UrbanDistrict d = parcel.getLocation().getAddress().getUrbanDistrict();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getName() + " " + d.getType();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelVillage(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.SovietVillage d = parcel.getLocation().getAddress().getSovietVillage();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getName() + " " + d.getType().value();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelLocality(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.Locality d = parcel.getLocation().getAddress().getLocality();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getName() + " " + d.getType();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelStreet(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.Street d = parcel.getLocation().getAddress().getStreet();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getName() + " " + d.getType();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelLevel1(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.Level1 d = parcel.getLocation().getAddress().getLevel1();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getType().value() + " " + d.getValue();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelLevel2(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.Level2 d = parcel.getLocation().getAddress().getLevel2();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getType().value() + " " + d.getValue();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelLevel3(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.Level3 d = parcel.getLocation().getAddress().getLevel3();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getType().value() + " " + d.getValue();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelFlat(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			final TAddress.Apartment d = parcel.getLocation().getAddress().getApartment();
			if (d == null)
				return LandProcessorConstants.NODATA;
			return d.getType().value() + " " + d.getValue();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelOther(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress().getOther() == null)
				return LandProcessorConstants.NODATA;
			return parcel.getLocation().getAddress().getOther();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelNote(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getAddress().getNote() == null)
				return LandProcessorConstants.NODATA;
			return parcel.getLocation().getAddress().getNote();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}

	private String getParcelNote2(TParcel parcel) {
		try {
			if (parcel.getLocation() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getElaboration() == null)
				return LandProcessorConstants.NODATA;
			if (parcel.getLocation().getElaboration().getReferenceMark() == null)
				return LandProcessorConstants.NODATA;
			return parcel.getLocation().getElaboration().getReferenceMark();
		} catch (Exception ex) {
			logger.error(ex);
			return LandProcessorConstants.NODATA;
		}
	}
}
