package org.tomskgislab.cadastre.xml;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.tomskgislab.landprocessor.shema.stdkv5.EntitySpatial;
import org.tomskgislab.landprocessor.shema.stdkv5.TSPATIALELEMENT;
import org.tomskgislab.landprocessor.shema.stdkv5.TSPELEMENTUNIT;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.CoordinateSequenceFactory;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequenceFactory;
import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: filippov
 * Date: 17.07.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
public class CadastreEntitySpatialKV05 {
    private static Logger logger = LogManager.getLogger(CadastreEntitySpatialKV.class);
    private GeometryFactory geometryFactory;

    // Основной, внешний контур
    private Polygon polygon;

    private EntitySpatial spatialData;

    public CadastreEntitySpatialKV05(EntitySpatial SpatialData) {
        spatialData = SpatialData;
        geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
        polygon = null;
        parseData();
    }

    private void parseData(){
        LinearRing mainRing = null;
        LinearRing[] holes;
        try{
            if (spatialData == null) return;

            List<TSPATIALELEMENT> SpatailElements = spatialData.getSpatialElement();
            holes = new LinearRing[SpatailElements.size()-1];
            for(int i = 0; i<SpatailElements.size(); i++){
                if (i == 0){
                    mainRing = createLinearRing(SpatailElements.get(i));
                }
                else {
                    holes[i-1] = createLinearRing(SpatailElements.get(i));
                }
            }
            polygon = new Polygon(mainRing, holes, this.geometryFactory);
        }
        catch(Exception ex){
            logger.error(ex.getLocalizedMessage());
        }
    }

    /** Создаёт замкнутый конур на основе списка (CoordinateList) координат
     * @return Контур JTS
     */
    private LinearRing createLinearRing(TSPATIALELEMENT SpatailElement) {
        List<TSPELEMENTUNIT> SpatialUnits = SpatailElement.getSpelementUnit();
        CoordinateList coords = new CoordinateList();
        for(int i=0; i<SpatialUnits.size(); i++){
            BigDecimal x = SpatialUnits.get(i).getOrdinate().getX();
            BigDecimal y = SpatialUnits.get(i).getOrdinate().getY();
            Coordinate coord = new Coordinate(y.doubleValue(), x.doubleValue());
            coords.add(coord);
        }

        coords.add(coords.get(0));
        Coordinate[] arr = coords.toCoordinateArray();
        try {
            CoordinateSequenceFactory fact = CoordinateArraySequenceFactory.
                    instance();
            CoordinateSequence cseq = fact.create(arr);
            LinearRing ring = new LinearRing(cseq, this.geometryFactory);
            return ring;
        } catch (Exception ex) {
            logger.error("Ошибка создания LinearRing. ", ex);
            return null;
        }
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public GeometryFactory getGeometryFactory() {
        return geometryFactory;
    }
}
