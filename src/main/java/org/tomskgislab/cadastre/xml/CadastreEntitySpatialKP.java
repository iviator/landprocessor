package org.tomskgislab.cadastre.xml;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.tomskgislab.landprocessor.shema.stdkp.EntitySpatial;
import org.tomskgislab.landprocessor.shema.stdkp.TSPATIALELEMENT;
import org.tomskgislab.landprocessor.shema.stdkp.TSPELEMENTUNIT;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.CoordinateSequenceFactory;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequenceFactory;

/**
 * Пространственная составляющая - Полигон с внутренними контурами
 * @author filippov
 * @since 1.0
 */
public class CadastreEntitySpatialKP {
	private static Logger logger = LogManager.getLogger(CadastreEntitySpatialKP.class);
	private GeometryFactory geometryFactory;
	
	// Основной, внешний контур
	private Polygon polygon;

	private EntitySpatial spatialData;
	
	public CadastreEntitySpatialKP(EntitySpatial SpatialData) {
		spatialData = SpatialData;
		geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
		polygon = null;
		parseData();
	}
	
	private void parseData(){
		LinearRing mainRing = null;
		LinearRing[] holes;
		try{
			if (spatialData == null) return;
			
			List<TSPATIALELEMENT> SpatailElements = spatialData.getSpatialElement();
			holes = new LinearRing[SpatailElements.size()-1];
			for(int i = 0; i<SpatailElements.size(); i++){
				if (i == 0){
					mainRing = createLinearRing(SpatailElements.get(i));
				}
				else {
					holes[i-1] = createLinearRing(SpatailElements.get(i));
				}
			}
			polygon = new Polygon(mainRing, holes, this.geometryFactory);
		}
		catch(Exception ex){
			logger.error(ex.getLocalizedMessage());
		}
	}
	
    /** Создаёт замкнутый конур на основе списка (CoordinateList) координат
    * @return Контур JTS
    */
   private LinearRing createLinearRing(TSPATIALELEMENT SpatailElement) {
    	List<TSPELEMENTUNIT> SpatialUnits = SpatailElement.getSpelementUnit();
    	CoordinateList coords = new CoordinateList();
    	for(int i=0; i<SpatialUnits.size(); i++){
    		BigDecimal x = SpatialUnits.get(i).getOrdinate().get(0).getX();
    		BigDecimal y = SpatialUnits.get(i).getOrdinate().get(0).getY();
    		Coordinate coord = new Coordinate(y.doubleValue(), x.doubleValue());
    		coords.add(coord);
    	}
    	
    	coords.add(coords.get(0));
	   Coordinate[] arr = coords.toCoordinateArray();
	   try {
	       CoordinateSequenceFactory fact = CoordinateArraySequenceFactory.
	               instance();
	       CoordinateSequence cseq = fact.create(arr);
	       LinearRing ring = new LinearRing(cseq, this.geometryFactory);
	       return ring;
	   } catch (Exception ex) {
	       logger.error("Ошибка создания LinearRing. ", ex);
	       return null;
	   }
   }
   
   public Polygon getPolygon() {
	   return polygon;
   }
   
   public GeometryFactory getGeometryFactory() {
		return geometryFactory;
	}
}