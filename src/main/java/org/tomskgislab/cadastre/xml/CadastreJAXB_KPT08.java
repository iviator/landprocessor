package org.tomskgislab.cadastre.xml;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.data.store.ReTypingIterator;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.tomskgislab.landprocessor.shema.stdkpt8.*;
import org.tomskgislab.landprocessor.shema.stdkpt8.TCadastralBlock.Zones.Zone;
import org.tomskgislab.landprocessor.shema.stdkpt8.TCadastralBlock.Bounds.Bound;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.tomskgislab.landprocessor.shema.share.CadastreDictionaryLoader;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
/**
 * Created with IntelliJ IDEA.
 * User: filippov
 * Date: 17.07.13
 * Time: 14:16
 * To change this template use File | Settings | File Templates.
 */
public class CadastreJAXB_KPT08 {
    private static Logger logger = LogManager.getLogger(CadastreJAXB_KPT08.class);

    // Участки
    private Parcels parcels;
    private TCadastralBlock quartal;
    //
    private CadastreDictionaryLoader dicts;
    private MultiPolygon parcelGeometry;

    private SimpleFeatureBuilder parcelBuilder;
    private SimpleFeatureBuilder quartalBuilder;
    private SimpleFeatureBuilder zoneBuilder;
    private SimpleFeatureBuilder boundBuilder;
    private DefaultFeatureCollection parcelCollection;
    private DefaultFeatureCollection quartallCollection;
    private DefaultFeatureCollection zoneCollection;
    private DefaultFeatureCollection boundCollection;

    public CadastreJAXB_KPT08(File Path,
                            DefaultFeatureCollection ParcelCollection,
                            DefaultFeatureCollection QartalCollection,
                            DefaultFeatureCollection ZoneCollection,
                            DefaultFeatureCollection BoundCollection,
                            SimpleFeatureBuilder ParcelBuilder,
                            SimpleFeatureBuilder QuartalBuilder,
                            SimpleFeatureBuilder ZoneBuilder,
                            SimpleFeatureBuilder BoundBuilder,
                            CadastreDictionaryLoader Dicts) {
        RegionCadastr kpt;
        try {
            this.dicts = Dicts;
            this.parcelBuilder = ParcelBuilder;
            this.quartalBuilder = QuartalBuilder;
            this.zoneBuilder = ZoneBuilder;
            this.boundBuilder = BoundBuilder;
            this.parcelCollection = ParcelCollection;
            this.quartallCollection = QartalCollection;
            this.zoneCollection = ZoneCollection;
            this.boundCollection = BoundCollection;
            JAXBContext context = JAXBContext.newInstance(RegionCadastr.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(Path);
            kpt = (RegionCadastr) obj;
            if (kpt != null) {
                parcels = kpt.getPackage().getCadastralBlocks().getCadastralBlock().get(0).getParcels();
                quartal = kpt.getPackage().getCadastralBlocks().getCadastralBlock().get(0);
                
                parse();
                // Походу нет частей в КПТ
                // parseSubParcel();
            }
        } catch (JAXBException ex) {
            logger.error(ex.getLocalizedMessage());
        }
    }

    private void parse() {
        try {

            // В этой версии появились границы зон и квартала

            // Квартал. Судя по .getEntitySpatial() не может быть только многоконтурным
            try {
                CadastreEntitySpatialKPT08 cesQ =
                        new CadastreEntitySpatialKPT08(quartal.getSpatialData().getEntitySpatial());

                Polygon quartalGeometry = cesQ.getPolygon();
                if (quartalGeometry != null) {

                    this.quartalBuilder.add(quartalGeometry);
                    this.quartalBuilder.add("QuartalFromKPT");
                    this.quartalBuilder.add(getQartalArea());
                    this.quartalBuilder.add(getQartalNote());
                    this.quartalBuilder.add(getQartalCoordSys());
                    this.quartalBuilder.add(getQartalCadastreNumber());
                    SimpleFeature featureQ = this.quartalBuilder
                            .buildFeature(null);
                    this.quartallCollection.add(featureQ);
                    logger.info("Кадастровый квартал создан.");
                }
            } catch (Exception ex) {
               logger.error("Ошибка создания квартала. " + ex.getMessage());
            }
            // TODO При наличии КПТ с зонами дописать их обработку
            
            try {
                if((quartal.getZones() != null) && (quartal.getZones().getZone().size() > 0)) {
                    for(int j = 0; j < quartal.getZones().getZone().size(); j++) {
                        Zone zone = quartal.getZones().getZone().get(j);
                        CadastreEntitySpatialKPT08 cesZ =
                                new CadastreEntitySpatialKPT08(zone.getEntitySpatial());

                        Polygon zoneGeometry = cesZ.getPolygon();

                        if (zoneGeometry != null) {
                            this.zoneBuilder.add(zoneGeometry);
                            this.zoneBuilder.add("Zone");
                            this.zoneBuilder.add(getZoneAccountNumber(zone));
                            this.zoneBuilder.add(getZoneDocCode(zone));
                            this.zoneBuilder.add(getZoneDocName(zone));
                            this.zoneBuilder.add(getZoneDocNumber(zone));
                            this.zoneBuilder.add(getZoneDocDate(zone));
                            this.zoneBuilder.add(getZoneDocIssue(zone));

                            this.zoneBuilder.add(getSpecZoneRestrict(zone));

                            this.zoneBuilder.add(getTerrZoneLanddUse(zone));
                            this.zoneBuilder.add(getTerrZoneTypePermittesUse(zone));
                            this.zoneBuilder.add(getTerrZonePermittedUse(zone));

                            SimpleFeature featureZ = this.zoneBuilder
                                    .buildFeature(null);
                            this.zoneCollection.add(featureZ);
                            logger.info("Зона создана.");
                        }
                    }
                }
        	} catch (Exception ex) {
        		logger.error("Ошибка создания зон. " + ex.getMessage());
        	}
            try {
                if ((quartal.getBounds() != null) && (quartal.getBounds().getBound().size() > 0)) {
                    for(int j = 0; j < quartal.getBounds().getBound().size(); j++) {
                        Bound bound = quartal.getBounds().getBound().get(j);
//TODO: Process all boundaries
                        CadastreEntitySpatialKPT08 cesZ =
                                new CadastreEntitySpatialKPT08(bound.getBoundaries().getBoundary().get(0).getEntitySpatial());

                        Polygon boundGeometry = cesZ.getPolygon();

                        if (boundGeometry != null) {
                            this.boundBuilder.add(boundGeometry);
                            this.boundBuilder.add("Bound");
                            this.boundBuilder.add(getBoundAccountNumber(bound));
                            this.boundBuilder.add(getBoundDocCode(bound));
                            this.boundBuilder.add(getBoundDocName(bound));
                            this.boundBuilder.add(getBoundDocNumber(bound));
                            this.boundBuilder.add(getBoundDocDate(bound));
                            this.boundBuilder.add(getBoundDocIssue(bound));

                            if (bound.getMunicipalBoundary() != null) {
                                this.boundBuilder.add("MunicipalBoundary");
                                this.boundBuilder.add(bound.getMunicipalBoundary().getName());
                            } else if (bound.getInhabitedLocalityBoundary() != null) {
                                this.boundBuilder.add("InhabitedLocalityBoundary");
                                this.boundBuilder.add(bound.getInhabitedLocalityBoundary().getName());
                            } else {
                                this.boundBuilder.add("Unknown");
                                this.boundBuilder.add("");
                            }

                            SimpleFeature featureB = this.boundBuilder
                                    .buildFeature(null);
                            this.boundCollection.add(featureB);
                            logger.info("Граница создана.");
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("Ошибка создания зон. " + ex.getMessage());
            }
            if(parcels != null) {
                for (int j = 0; j < parcels.getParcel().size(); j++) {
                    TParcel parcel = parcels.getParcel().get(j);
                    logger.info("Начало обработки ЗУ "
                            + this.getCadastreNumber(parcel));
                    if (parcel != null) {
                        try {
                            // Одноконтурный
                            if (parcel.getContours() == null) {
                                CadastreEntitySpatialKPT08 ces = new CadastreEntitySpatialKPT08(
                                        parcel.getEntitySpatial());
                                Polygon p = ces.getPolygon();
                                if (p == null) {
                                    logger.warn("Геометри контура ЗУ "
                                            + this.getCadastreNumber(parcel)
                                            + " пустая!");
                                    continue;
                                }
                                Polygon[] pa = new Polygon[1];
                                pa[0] = p;
                                parcelGeometry = new MultiPolygon(pa,
                                        GeometryBuilder.getGeometryFactory());
                            }
                            // Многоконтурный
                            else if (parcel.getContours().getContour().size() > 0) {

                                Polygon[] pa = new Polygon[parcel.getContours()
                                        .getContour().size()];
                                for (int i = 0; i < parcel.getContours().getContour()
                                        .size(); i++) {
                                    CadastreEntitySpatialKPT08 ces = new CadastreEntitySpatialKPT08(
                                            parcel.getContours().getContour().get(i)
                                                    .getEntitySpatial());
                                    Polygon p = ces.getPolygon();
                                    pa[i] = p;
                                    int k=i+1;
                                    logger.info("Многоконтурная геометрия. Контур " + k);
                                }
                                parcelGeometry = new MultiPolygon(pa,
                                        JTSFactoryFinder.getGeometryFactory(null));
                            }
                        }
                        catch(Exception ex) {
                            logger.error(ex);
                            return;
                        }

                        // Добавлять значения в поля в том порядке, в каком
                        // добавлены поля
                        // в целевом шейпе.
    //					if (parcelGeometry.isValid()) {
                        this.parcelBuilder.add(parcelGeometry);
                        this.parcelBuilder.add("ParcelsFromKPT");
                        this.parcelBuilder.add(getCadastreNumber(parcel));
                        this.parcelBuilder.add(getUtilizationByDoc(parcel));
                        this.parcelBuilder.add(getUtilizationByType(parcel));
                        this.parcelBuilder.add(getParcelType(parcel));
                        this.parcelBuilder.add(getParcelState(parcel));

                        // Могут быть 2 права (Собственность у РФ и ПБП у
                        // какой-нибудь госконторы)
                        this.parcelBuilder.add(getParcelRigth1(parcel));
                        this.parcelBuilder.add(getParcelRigth2(parcel));

                        this.parcelBuilder.add(this.getCategory(parcel));

                        // в квадратных метрах
                        this.parcelBuilder.add(getParcelArea(parcel));
                        this.parcelBuilder.add(getParcelAreaUnit(parcel));

                        this.parcelBuilder.add(LandProcessorConstants.NODATA);
                        this.parcelBuilder.add(LandProcessorConstants.NODATA);
                        this.parcelBuilder.add(LandProcessorConstants.NODATA);
                        this.parcelBuilder.add(LandProcessorConstants.NODATA);
                        this.parcelBuilder.add(LandProcessorConstants.NODATA);
                        this.parcelBuilder.add(LandProcessorConstants.NODATA);
                        this.parcelBuilder.add(LandProcessorConstants.NODATA);

                        this.parcelBuilder.add(getParcelRegion(parcel));
                        this.parcelBuilder.add(getParcelParish(parcel));
                        this.parcelBuilder.add(getParcelCity(parcel));
                        this.parcelBuilder.add(getParcelDistrict(parcel));
                        this.parcelBuilder.add(getParcelVillage(parcel));
                        this.parcelBuilder.add(getParcelLocality(parcel));
                        this.parcelBuilder.add(getParcelStreet(parcel));
                        this.parcelBuilder.add(getParcelLevel1(parcel));
                        this.parcelBuilder.add(getParcelLevel2(parcel));
                        this.parcelBuilder.add(getParcelLevel3(parcel));
                        this.parcelBuilder.add(getParcelFlat(parcel));

                        this.parcelBuilder.add(getParcelOther(parcel));
                        this.parcelBuilder.add(getParcelNote(parcel));
                        this.parcelBuilder.add(getParcelNote2(parcel));

                        SimpleFeature feature = this.parcelBuilder
                                .buildFeature(null);
                        this.parcelCollection.add(feature);
    //					} else {
    //						logger.info("Геометрия не прошла проверку  "
    //								+ this.getCadastreNumber(parcel));
    //					}
                    }
                    logger.info("Конец обработки ЗУ  "
                            + this.getCadastreNumber(parcel));
                    }
                }
            } catch (Exception ex) {
                logger.error(ex.getLocalizedMessage());
            }
    }

    private String getCategory(TParcel parcel) {
        String Key = parcel.getCategory().getCategory();
        return dicts.getValueFromDictionary(LandProcessorConstants.dCategories,
                Key);
    }

    private String getCadastreNumber(TParcel parcel) {
        try {
            if (parcel.getCadastralNumber().indexOf(":") == 0)
                return quartal.getCadastralNumber() + parcel.getCadastralNumber();
            if (parcel.getCadastralNumber().indexOf(":") == 2)
                return parcel.getCadastralNumber();
            return LandProcessorConstants.NODATA;
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getCadastreNumberPart(TSubParcel Part, TParcel parcel) {
        try {
            return getCadastreNumber(parcel) + "/" + Part.getNumberRecord();
            // TODO Разобраться
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getUtilizationByDoc(TParcel parcel) {
        try {
            return parcel.getUtilization().getByDoc();
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getUtilizationByType(TParcel parcel) {
        // Тут нужно значение по типу из справочника
        try {
            String kind = parcel.getUtilization().getKind();
            return dicts.getValueFromDictionary(
                    LandProcessorConstants.dUtilizations, kind);
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelType(TParcel parcel) {
        // Тут нужно значение по типу из справочника dParcels.xsd
        try {
            switch (parcel.getName()) {
                case "01":
                    return LandProcessorConstants.PARCEL_TYPE_01;
                case "02":
                    return LandProcessorConstants.PARCEL_TYPE_02;
                case "03":
                    return LandProcessorConstants.PARCEL_TYPE_03;
                case "04":
                    return LandProcessorConstants.PARCEL_TYPE_04;
                case "05":
                    return LandProcessorConstants.PARCEL_TYPE_05;
                case "06":
                    return LandProcessorConstants.PARCEL_TYPE_06;
                default:
                    return LandProcessorConstants.NODATA;
            }
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelState(TParcel parcel) {
        // Тут нужно значение по типу из справочника
        try {
            String stateKey = parcel.getState();
            return dicts.getValueFromDictionary(LandProcessorConstants.dStates,
                    stateKey);
        }
        catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    // иногда информации по правам нет
    private List<TRight> getRightsNode(TParcel parcel) {
        if ((parcel.getRights() != null) &&
                (parcel.getRights().getRight() != null)) {
            return parcel.getRights().getRight();
        }
        logger.warn("Нет информации о правах");
        return null;
    }

    private String getParcelRigth1(TParcel parcel) {
        try {
            if (getRightsNode(parcel) != null) {
                String RigthKey = getRightsNode(parcel).get(0).getType();
                return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
                        RigthKey);
            }
            return LandProcessorConstants.NODATA;
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelRigth2(TParcel parcel) {
        try {
            if((getRightsNode(parcel) != null) && (getRightsNode(parcel).size() > 1)) {
                String RigthKey = getRightsNode(parcel).get(1).getType();
                return dicts.getValueFromDictionary(LandProcessorConstants.dRights,
                        RigthKey);
            }
            return LandProcessorConstants.NODATA;
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelArea(TParcel parcel) {
        try {
            return parcel.getArea().getArea().toString();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelAreaUnit(TParcel parcel) {
        try {
            String AreaUnitKey = parcel.getArea().getUnit();
            return dicts.getValueFromDictionary(LandProcessorConstants.dUnit,
                    AreaUnitKey);
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelRegion(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress().getOther() == null)
                return LandProcessorConstants.NODATA;
            final String regionKey = parcel.getLocation().getAddress().getRegion();
            return dicts.getValueFromDictionary(LandProcessorConstants.dRegions, regionKey);
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelParish(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.District d = parcel.getLocation().getAddress().getDistrict();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelCity(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.City d = parcel.getLocation().getAddress().getCity();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelDistrict(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.UrbanDistrict d = parcel.getLocation().getAddress().getUrbanDistrict();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelVillage(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.SovietVillage d = parcel.getLocation().getAddress().getSovietVillage();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType().value();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLocality(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Locality d = parcel.getLocation().getAddress().getLocality();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelStreet(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Street d = parcel.getLocation().getAddress().getStreet();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getName() + " " + d.getType();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLevel1(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Level1 d = parcel.getLocation().getAddress().getLevel1();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLevel2(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Level2 d = parcel.getLocation().getAddress().getLevel2();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelLevel3(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Level3 d = parcel.getLocation().getAddress().getLevel3();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelFlat(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            final TAddress.Apartment d = parcel.getLocation().getAddress().getApartment();
            if (d == null)
                return LandProcessorConstants.NODATA;
            return d.getType().value() + " " + d.getValue();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelOther(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress().getOther() == null)
                return LandProcessorConstants.NODATA;
            return parcel.getLocation().getAddress().getOther();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelNote(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getAddress().getNote() == null)
                return LandProcessorConstants.NODATA;
            return parcel.getLocation().getAddress().getNote();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getParcelNote2(TParcel parcel) {
        try {
            if (parcel.getLocation() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getElaboration() == null)
                return LandProcessorConstants.NODATA;
            if (parcel.getLocation().getElaboration().getReferenceMark() == null)
                return LandProcessorConstants.NODATA;
            return parcel.getLocation().getElaboration().getReferenceMark();
        } catch (Exception ex) {
            logger.error(ex);
            return LandProcessorConstants.NODATA;
        }
    }

    private String getQartalArea() {
        try {
             if (quartal.getArea() != null)
                 return quartal.getArea().getTotal().toString();
             return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getQartalNote() {
        try {
            if (!quartal.getNote().equals(""))
                return quartal.getNote();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getQartalCoordSys() {
        try {
            if (quartal.getCoordSystem() != null)
                return quartal.getCoordSystem().getName();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getQartalCadastreNumber() {
        try {
            if (!quartal.getCadastralNumber().equals(""))
                return quartal.getCadastralNumber();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneDocCode(Zone zone) {
        try {
            if (zone.getDocuments().getDocument().get(0)!=null) {
            	String codeKey = zone.getDocuments().getDocument().get(0).getCodeDocument();
                return dicts.getValueFromDictionary(LandProcessorConstants.dAllDocuments,
                		codeKey);
            }
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneDocName(Zone zone) {
        try {
            if (zone.getDocuments().getDocument().get(0) != null)
            	return zone.getDocuments().getDocument().get(0).getName();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneDocNumber(Zone zone) {
        try {
            if (zone.getDocuments().getDocument().get(0) != null)
            	return zone.getDocuments().getDocument().get(0).getNumber();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneDocDate(Zone zone) {
        try {
            if (zone.getDocuments().getDocument().get(0) != null)
            	return zone.getDocuments().getDocument().get(0).getDate().toString();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneDocSeries(Zone zone) {
        try {
            if (zone.getDocuments().getDocument().get(0) != null)
            	return zone.getDocuments().getDocument().get(0).getSeries();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneDocIssue(Zone zone) {
        try {
            if (zone.getDocuments().getDocument().get(0) != null)
            	return zone.getDocuments().getDocument().get(0).getIssueOrgan();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getZoneAccountNumber(Zone zone) {
        try {
            return zone.getAccountNumber();
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getSpecZoneRestrict(Zone zone) {
        try {
            if (zone.getSpecialZone() != null)
            	return zone.getSpecialZone().getContentRestrictions();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getTerrZonePermittedUse(Zone zone) {
        try {
            if (zone.getTerritorialZone() != null)
            	return zone.getTerritorialZone().getPermittedUses().getPermittedUse().get(0).getPermittedUse();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getTerrZoneLanddUse(Zone zone) {
        try {
            if (zone.getTerritorialZone() != null) {
            	String useKey = zone.getTerritorialZone().getPermittedUses().getPermittedUse().get(0).getLandUse();
                return dicts.getValueFromDictionary(LandProcessorConstants.dUtilizations,
                		useKey);
            }
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getTerrZoneTypePermittesUse(Zone zone) {
        try {
            if (zone.getTerritorialZone() != null) {
            	String useKey = zone.getTerritorialZone().getPermittedUses().getPermittedUse().get(0).getTypePermittedUse();
                return dicts.getValueFromDictionary(LandProcessorConstants.dPermitUse,
                		useKey);
            }
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
    
    private String getBoundDocCode(Bound bound) {
        try {
            if (bound.getDocuments().getDocument().get(0) != null) {
                String codeKey = bound.getDocuments().getDocument().get(0).getCodeDocument();
                return dicts.getValueFromDictionary(LandProcessorConstants.dAllDocuments, codeKey);
            }
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getBoundDocName(Bound bound) {
        try {
            if (bound.getDocuments().getDocument().get(0) != null)
                return bound.getDocuments().getDocument().get(0).getName();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getBoundDocNumber(Bound bound) {
        try {
            if (bound.getDocuments().getDocument().get(0) != null)
                return bound.getDocuments().getDocument().get(0).getNumber();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getBoundDocDate(Bound bound) {
        try {
            if (bound.getDocuments().getDocument().get(0) != null)
                return bound.getDocuments().getDocument().get(0).getDate().toString();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getBoundDocSeries(Bound bound) {
        try {
            if (bound.getDocuments().getDocument().get(0) != null)
                return bound.getDocuments().getDocument().get(0).getSeries();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getBoundDocIssue(Bound bound) {
        try {
            if (bound.getDocuments().getDocument().get(0) != null)
                return bound.getDocuments().getDocument().get(0).getIssueOrgan();
            return LandProcessorConstants.NODATA;
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }

    private String getBoundAccountNumber(Bound bound) {
        try {
            return bound.getAccountNumber();
        } catch(Exception ex) {
            return LandProcessorConstants.NODATA;
        }
    }
}
