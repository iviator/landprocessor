package org.tomskgislab.cadastre.xml;

import java.io.File;

import javax.swing.text.ZoneView;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.referencing.crs.DefaultEngineeringCRS;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.tomskgislab.landprocessor.CadastreFeatureBuilder;
import org.tomskgislab.landprocessor.CadastreFeatureBuilder.CadastreFeatureTypes;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.tomskgislab.landprocessor.ex.LandProcessorException;
import org.tomskgislab.landprocessor.shema.share.CadastreDictionaryLoader;
import org.w3c.dom.Document;

public class CadastreMainJAXB {
	
	private static Logger logger = LogManager.getLogger(CadastreMainJAXB.class);
	private DefaultFeatureCollection parcelCollection;
    private DefaultFeatureCollection quartalCollection;
	private DefaultFeatureCollection pointCollection;
	private DefaultFeatureCollection zoneCollection;
	private DefaultFeatureCollection boundCollection;
	private SimpleFeatureType PARCEL_TYPE;
    private SimpleFeatureType QUARTAL_TYPE;
	private SimpleFeatureType POINT_TYPE;
	private SimpleFeatureType ZONE_TYPE;
	private SimpleFeatureType BOUND_TYPE;

	public CadastreMainJAXB(File Path, CadastreDictionaryLoader dicts) throws Exception {
		
//		One constant deserves special mention as it is used as a “wild card”
//		placeholder for when you are unsure of your data. The concept of a “Generic 2D” 
//		CoordianteReferenceSystem is formally intended for working with things 
//		like CAD drawings where the results are measured in meters.

		CoordinateReferenceSystem crs = DefaultEngineeringCRS.GENERIC_2D;// (EPSG, false);

		PARCEL_TYPE = CadastreFeatureBuilder.createFeatureType(crs,
				CadastreFeatureTypes.Parcel);
		POINT_TYPE = CadastreFeatureBuilder.createFeatureType(crs,
				CadastreFeatureTypes.CharacterPoint);
        QUARTAL_TYPE = CadastreFeatureBuilder.createFeatureType(crs,
                CadastreFeatureTypes.Quartal);
        ZONE_TYPE = CadastreFeatureBuilder.createFeatureType(crs,
                CadastreFeatureTypes.Zones);
        BOUND_TYPE = CadastreFeatureBuilder.createFeatureType(crs,
                CadastreFeatureTypes.Bounds);

		parcelCollection = new DefaultFeatureCollection("internal", PARCEL_TYPE);
		pointCollection = new DefaultFeatureCollection("internal", POINT_TYPE);
        quartalCollection = new DefaultFeatureCollection("internal", QUARTAL_TYPE);
        zoneCollection = new DefaultFeatureCollection("internal", ZONE_TYPE);
        boundCollection = new DefaultFeatureCollection("internal", BOUND_TYPE);

		SimpleFeatureBuilder featureBuilderParcel = new SimpleFeatureBuilder(PARCEL_TYPE);
        SimpleFeatureBuilder featureBuilderQuartal = new SimpleFeatureBuilder(QUARTAL_TYPE);
		SimpleFeatureBuilder featureBuilderPoint = new SimpleFeatureBuilder(POINT_TYPE);
		SimpleFeatureBuilder featureBuilderZone = new SimpleFeatureBuilder(ZONE_TYPE);
		SimpleFeatureBuilder featureBuilderBound = new SimpleFeatureBuilder(BOUND_TYPE);
		
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		DocumentBuilder builder;

		builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(Path);
		// КВЗУ
		if ((doc.getFirstChild().getNextSibling().getNodeName().equals((LandProcessorConstants.KV)))
				|| (doc.getFirstChild().getNextSibling().getNodeName().equals((LandProcessorConstants.KV1))))

		{
			// Version 05
			if (doc.getFirstChild().getNextSibling().getAttributes().getNamedItem("Version") != null &&
			    doc.getFirstChild().getNextSibling().getAttributes().getNamedItem("Version").getNodeValue().equals("05")) {

				CadastreJAXB_KV05 parser = new CadastreJAXB_KV05(Path, parcelCollection, featureBuilderParcel, dicts);
			}
			// Version 04
			else if (doc.getFirstChild().getNextSibling().getFirstChild().getNodeName().equals("eDocument") &&
					doc.getFirstChild().getNextSibling().getFirstChild().getAttributes().getNamedItem("Version").getNodeValue().equals("04")) {

				CadastreJAXB_KV04 parser = new CadastreJAXB_KV04(Path, parcelCollection, featureBuilderParcel, dicts);
			} else
				throw new LandProcessorException("Unknown KV format");
		}
			
		// КПТ
		else if (doc.getFirstChild().getNextSibling().getNodeName().equals((LandProcessorConstants.KPT))) {
			// Version 08
			if (doc.getFirstChild().getNextSibling().getAttributes().getNamedItem("Version") != null &&
			    doc.getFirstChild().getNextSibling().getAttributes().getNamedItem("Version").getNodeValue().equals("08")) {

				logger.info("KPT08");
				CadastreJAXB_KPT08 parser = new CadastreJAXB_KPT08(Path, 
						parcelCollection, quartalCollection, zoneCollection, boundCollection,
                        featureBuilderParcel, featureBuilderQuartal, featureBuilderZone, featureBuilderBound,
                        dicts);
			}
			// Version 07
			else if (doc.getFirstChild().getNextSibling().getFirstChild().getNodeName().equals("eDocument") &&
				     doc.getFirstChild().getNextSibling().getFirstChild().getAttributes().getNamedItem("Version").getNodeValue().equals("07")) {
				logger.info("KPT07");
				CadastreJAXB_KPT07 parser = new CadastreJAXB_KPT07(Path, parcelCollection, featureBuilderParcel, dicts);
			} else {
				logger.info("KPT?");
				throw new LandProcessorException("Unknown KPT format");
			}
		}

		else if (doc.getFirstChild().getNextSibling().getNodeName().equals((LandProcessorConstants.KP))) {
            if (doc.getFirstChild().getNextSibling().getAttributes().getNamedItem("Version") != null &&
                    doc.getFirstChild().getNextSibling().getAttributes().getNamedItem("Version").getNodeValue().equals("04")) {
                logger.info("KP04");
                CadastreJAXB_KP04 parser = new CadastreJAXB_KP04(Path,
                        parcelCollection,featureBuilderParcel, dicts);
            }
            else {
                logger.info("KP03");
                CadastreJAXB_KP parser = new CadastreJAXB_KP(Path,
                        parcelCollection, featureBuilderParcel, dicts);
            }
		}
		else
			throw new LandProcessorException("Данный тип XML-файлов не обрабатывается, либо файл создан на основе старой XML-схемы.");
	}

	public SimpleFeatureCollection getParcelCollection() {
		return parcelCollection;
	}

    public SimpleFeatureCollection getQuartalCollection() {
        return quartalCollection;
    }
	
    public SimpleFeatureCollection getZoneCollection() {
        return zoneCollection;
    }
    
    public SimpleFeatureCollection getBoundCollection() {
        return boundCollection;
    }

	public SimpleFeatureType getPARCEL_TYPE() {
		return PARCEL_TYPE;
	}

    public SimpleFeatureType getQUARTAL_TYPE() {
        return QUARTAL_TYPE;
    }
    
    public SimpleFeatureType getZONEL_TYPE() {
        return ZONE_TYPE;
    }

    public SimpleFeatureType getBOUND_TYPE() {
        return BOUND_TYPE;
    }
}
