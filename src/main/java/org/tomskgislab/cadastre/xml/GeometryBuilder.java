package org.tomskgislab.cadastre.xml;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.geometry.jts.JTSFactoryFinder;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.CoordinateSequenceFactory;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequenceFactory;

public class GeometryBuilder {
	
	private static Logger logger = LogManager.getLogger(GeometryBuilder.class);
	private static GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
    //private static CoordinateList coords; // Координаты контура
    //private static LinearRing mainRing; // Основной контур
	   
    /**
     * Создаёт замкнутый конур на основе списка (CoordinateList) координат
     * @return Контур JTS
     */
    public static LinearRing createLinearRing(CoordinateList coords) {
        coords.add(coords.get(0));
        Coordinate[] arr = coords.toCoordinateArray();
        //coords.clear();
        try {
            CoordinateSequenceFactory fact = CoordinateArraySequenceFactory.
                    instance();
            CoordinateSequence cseq = fact.create(arr);
            LinearRing ring = new LinearRing(cseq, geometryFactory);
            // innerRingsCount++;
            return ring;
        } catch (Exception ex) {
            logger.error("Ошибка создания LinearRing. ", ex);
            return null;
        }
    }
    
    /**
     * Создаёт полигон на основе основного и списка внутренних контуров.
     * Содержит проверку на наличие в списке внутренних контуров контура,
     * который больше внешнего.
     * При наличии такого контура, он меняется местами с основным.
     * @param linearRing Список внутренних контуров
     * @return Полигон JTS
     */
    public static Polygon createPolygon(List<LinearRing> linearRing) {
        try {
        	LinearRing mainRing = null;;
            if ((linearRing != null) && (linearRing.size() > 0)) {
                int maxAreaIndex = 0;
                // Если один контур, то создаём сразу.
                if (linearRing.size() == 1) {
                	return new Polygon(linearRing.get(0), null, geometryFactory);
                }
                int h = linearRing.size()-1;
                logger.info("Обрабатывается полигон с 'дырками', количеством " + h);
                Polygon p = new Polygon(linearRing.get(0), null, geometryFactory);
                double maxArea = p.getArea();
                logger.info("Площадь 'основного' контура " + maxArea);
                for (int i = 1; i < linearRing.size(); i++) {
                    LinearRing lr = linearRing.get(i);
                    Polygon pHole = new Polygon(lr, null, geometryFactory);
                    logger.info("Площадь " + i +  "контура " + pHole.getArea());
                    if (maxArea < pHole.getArea()) {
                        maxArea = pHole.getArea();
                        maxAreaIndex = i;
                        logger.info("Основной контур теперь " + i);
                    }
                }
                
                mainRing = (LinearRing)linearRing.remove(maxAreaIndex);
                Polygon pp = new Polygon(mainRing, null, geometryFactory);
                logger.info("Площадь 'основного' контура " + pp.getArea());
                if (maxAreaIndex > 0) {
                    logger.info("Основной контур объекта в XML был в списке не первый! Основной контур заменён. " +
                    		"Индекс основного контура был " + maxAreaIndex);
                }
                LinearRing[] lra = new LinearRing[linearRing.size()];
                for (int i = 0; i < linearRing.size(); i++) {
                    lra[i] = linearRing.get(i);
                }
                return new Polygon(mainRing, lra, geometryFactory);
            }
            return null;
        } catch (Exception ex) {
            logger.error("Ошибка создания полигона. ", ex);
            return null;
        }
    }

	public static GeometryFactory getGeometryFactory() {
		return geometryFactory;
	}
}
