package org.tomskgislab.cadastre.xml;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.tomskgislab.landprocessor.shema.stdkpt8.EntitySpatial;
import org.tomskgislab.landprocessor.shema.stdkpt8.TSPATIALELEMENT;
import org.tomskgislab.landprocessor.shema.stdkpt8.TSPELEMENTUNIT;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.CoordinateSequenceFactory;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Created with IntelliJ IDEA.
 * User: filippov
 * Date: 17.07.13
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */
public class CadastreEntitySpatialKPT08 {
    private static Logger logger = LogManager
            .getLogger(CadastreEntitySpatialKPT.class);
    private GeometryFactory geometryFactory;

    // Основной, внешний контур
    private Polygon polygon;

    private EntitySpatial spatialData;

    public CadastreEntitySpatialKPT08(EntitySpatial SpatialData) {
        spatialData = SpatialData;
        geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
        polygon = null;
        parseData();
    }

    private void parseData() {
        List<LinearRing> rings;
        try {
            if (spatialData == null)
                return;

            List<TSPATIALELEMENT> SpatailElements = spatialData
                    .getSpatialElement();
            rings = new ArrayList< LinearRing>();
            for (int i = 0; i < SpatailElements.size(); i++) {
//				if (i == 0) {
//					mainRing = createLinearRing(SpatailElements.get(i));
//				} else {
//					holes[i - 1] = createLinearRing(SpatailElements.get(i));
//				}
                rings.add(createLinearRing(SpatailElements.get(i)));
            }
            //polygon = new Polygon(mainRing, holes, this.geometryFactory);
            polygon = GeometryBuilder.createPolygon(rings);
        } catch (Exception ex) {
            logger.error(ex.getLocalizedMessage());
        }
    }

    /**
     * Создаёт замкнутый конур на основе списка (CoordinateList) координат
     *
     * @return Контур JTS
     */
    private LinearRing createLinearRing(TSPATIALELEMENT SpatailElement) {
        try {
            List<TSPELEMENTUNIT> SpatialUnits = SpatailElement.getSpelementUnit();
            CoordinateList coords = new CoordinateList();
            for (int i = 0; i < SpatialUnits.size(); i++) {
                BigDecimal x = SpatialUnits.get(i).getOrdinate().getX();
                BigDecimal y = SpatialUnits.get(i).getOrdinate().getY();
                Coordinate coord = new Coordinate(y.doubleValue(), x.doubleValue());
                coords.add(coord);
            }
            return GeometryBuilder.createLinearRing(coords);
        }
        catch(Exception ex) {
            logger.error(ex);
            return null;
        }
    }

    public Polygon getPolygon() {
        return polygon;
    }

}
