package org.tomskgislab.landprocessor;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.mif.MIFDataStore;
import org.geotools.data.mif.MIFDataStoreFactory;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.tomskgislab.cadastre.xml.CadastreMainJAXB;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.tomskgislab.landprocessor.shema.share.CadastreDictionaryLoader;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class LandProcessor {
    private static Logger logger = LogManager.getLogger(LandProcessor.class);

    private SimpleFeatureType PARCEL_TYPE;
    private SimpleFeatureType QUARTAL_TYPE;
    private SimpleFeatureType ZONE_TYPE;
    private SimpleFeatureType BOUND_TYPE;
    private SimpleFeatureCollection parcelCollection;
    private SimpleFeatureCollection quartalCollection;
    private SimpleFeatureCollection zoneCollection;
    private SimpleFeatureCollection boundCollection;
    private CadastreDictionaryLoader dicts;
    private CadastreMainJAXB parser;
    private String err = "\nДанные обработаны с ошибками. Для более подробной инфориации см. landprocessor.log";

    public LandProcessor() {
        dicts = new CadastreDictionaryLoader(System.getProperty("user.dir")+ LandProcessorConstants.PATH_TO_XSD);
    }

    protected void ProcessXMLFile(File file) throws Exception {
		logger.info("Начало обработки, ждите.....");

		parser = new CadastreMainJAXB(file, dicts);
		
		parcelCollection = parser.getParcelCollection();
        quartalCollection = parser.getQuartalCollection();
        zoneCollection = parser.getZoneCollection();
        boundCollection = parser.getBoundCollection();
		PARCEL_TYPE = parser.getPARCEL_TYPE();
        QUARTAL_TYPE = parser.getQUARTAL_TYPE();
        ZONE_TYPE = parser.getZONEL_TYPE();
        BOUND_TYPE = parser.getBOUND_TYPE();
        if ((this.parcelCollection.size() == 0) &&
            (this.quartalCollection.size() == 0)){
            logger.info("Нет данных для конвертации.");
        } else {
		    logger.info("Данные подготовлены для сохранения.");
        }
    }

    protected void saveToShape(File FileName, String charset) throws IOException, FactoryException {
        if (this.parcelCollection.size() != 0) {
            File RenameParcelFile = new File(FileName.getPath().replaceAll(
                    ".shp", "")
                    + "_Parcel" + ".shp");
            FileName.renameTo(RenameParcelFile);
            ShapefileDataStoreFactory polygonDataStoreFactory = new ShapefileDataStoreFactory();

            Map<String, Serializable> paramsPolygon = new HashMap<String, Serializable>();
            paramsPolygon.put("url", RenameParcelFile.toURI().toURL());
            paramsPolygon.put("create spatial index", Boolean.TRUE);
            ShapefileDataStore polygonDataStore = (ShapefileDataStore) polygonDataStoreFactory
                    .createNewDataStore(paramsPolygon);
            SimpleFeatureType optType = createOptimizedType(parcelCollection, PARCEL_TYPE, 255);
            polygonDataStore.setStringCharset(Charset.forName(charset));
            polygonDataStore.createSchema(optType);
            String ParcelTypeName = polygonDataStore.getTypeNames()[0];
            SimpleFeatureSource parcelFeatureSource = polygonDataStore
                    .getFeatureSource(ParcelTypeName);

            if (parcelFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) parcelFeatureSource, parcelCollection);
            } else {
                logger.warn(ParcelTypeName + " не поддерживает чтение/запись");
            }

            logger.info("Завершение конвертации участков. " + RenameParcelFile.getAbsolutePath());
        }
        if (this.quartalCollection.size() != 0) {
            File RenameQuartalFile = new File(FileName.getPath().replaceAll(
                    ".shp", "") + "_Qartal" + ".shp");
            FileName.renameTo(RenameQuartalFile);
            ShapefileDataStoreFactory quartalDataStoreFactory = new ShapefileDataStoreFactory();

            Map<String, Serializable> paramsQuartal = new HashMap<String, Serializable>();
            paramsQuartal.put("url", RenameQuartalFile.toURI().toURL());
            paramsQuartal.put("create spatial index", Boolean.TRUE);
            ShapefileDataStore quartalDataStore = (ShapefileDataStore) quartalDataStoreFactory
                    .createNewDataStore(paramsQuartal);
            SimpleFeatureType QoptType = createOptimizedType(quartalCollection, QUARTAL_TYPE, 255);
            quartalDataStore.setStringCharset(Charset.forName(charset));
            quartalDataStore.createSchema(QoptType);
            String QuartalTypeName = quartalDataStore.getTypeNames()[0];
            SimpleFeatureSource quartalFeatureSource = quartalDataStore
                    .getFeatureSource(QuartalTypeName);

            if (quartalFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) quartalFeatureSource, quartalCollection);
            } else {
                logger.warn(QuartalTypeName + " не поддерживает чтение/запись");
            }

            logger.info("Завершение конвертации квартала. " + RenameQuartalFile.getAbsolutePath());
        }
        if (this.zoneCollection.size() != 0) {
            File RenameZoneFile = new File(FileName.getPath().replaceAll(
                    ".shp", "") + "_Zones" + ".shp");
            FileName.renameTo(RenameZoneFile);
            ShapefileDataStoreFactory zoneDataStoreFactory = new ShapefileDataStoreFactory();

            Map<String, Serializable> paramsZone = new HashMap<String, Serializable>();
            paramsZone.put("url", RenameZoneFile.toURI().toURL());
            paramsZone.put("create spatial index", Boolean.TRUE);
            ShapefileDataStore zoneDataStore = (ShapefileDataStore) zoneDataStoreFactory
                    .createNewDataStore(paramsZone);
            SimpleFeatureType ZoptType = createOptimizedType(zoneCollection, ZONE_TYPE, 255);
            zoneDataStore.setStringCharset(Charset.forName(charset));
            zoneDataStore.createSchema(ZoptType);
            String ZoneTypeName = zoneDataStore.getTypeNames()[0];
            SimpleFeatureSource zoneFeatureSource = zoneDataStore
                    .getFeatureSource(ZoneTypeName);

            if (zoneFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) zoneFeatureSource, zoneCollection);
            } else {
                logger.warn(ZoneTypeName + " не поддерживает чтение/запись");
            }

            logger.info("Завершение конвертации зон. " + RenameZoneFile.getAbsolutePath());
        }
        if (this.boundCollection.size() != 0) {
            File RenameBoundFile = new File(FileName.getPath().replaceAll(
                    ".shp", "") + "_Bounds" + ".shp");
            FileName.renameTo(RenameBoundFile);
            ShapefileDataStoreFactory boundDataStoreFactory = new ShapefileDataStoreFactory();

            Map<String, Serializable> paramsBound = new HashMap<String, Serializable>();
            paramsBound.put("url", RenameBoundFile.toURI().toURL());
            paramsBound.put("create spatial index", Boolean.TRUE);
            ShapefileDataStore boundDataStore = (ShapefileDataStore) boundDataStoreFactory
                    .createNewDataStore(paramsBound);
            SimpleFeatureType BoptType = createOptimizedType(boundCollection, BOUND_TYPE, 255);
            boundDataStore.setStringCharset(Charset.forName(charset));
            boundDataStore.createSchema(BoptType);
            String BoundTypeName = boundDataStore.getTypeNames()[0];
            SimpleFeatureSource boundFeatureSource = boundDataStore
                    .getFeatureSource(BoundTypeName);

            if (boundFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) boundFeatureSource, boundCollection);
            } else {
                logger.warn(BoundTypeName + " не поддерживает чтение/запись");
            }

            logger.info("Завершение конвертации границ. " + RenameBoundFile.getAbsolutePath());
        }
    }

    private MIFDataStore createMIFDataStore(File FileName, ReferencedEnvelope bounds) throws IOException, FactoryException {
        MIFDataStoreFactory polygonDataStoreFactory = new MIFDataStoreFactory();

        Map<String, Serializable> params = new HashMap<String, Serializable>();
        params.put("dbtype", "mif");
        params.put("path", FileName.getAbsolutePath());
        params.put(MIFDataStore.PARAM_FIELDCASE, "upper");
        params.put(MIFDataStore.PARAM_GEOMNAME, "the_geom");
        params.put(MIFDataStore.PARAM_GEOMTYPE, "typed");
        params.put(MIFDataStore.HCLAUSE_CHARSET, "Neutral");
        params.put(MIFDataStore.HCLAUSE_COORDSYS, "NonEarth Units \"m\" Bounds (" + bounds.getMinimum(0) +", " + bounds.getMinimum(1) + ") (" + bounds.getMaximum(0) +", " + bounds.getMaximum(1) + ")");

        return (MIFDataStore) polygonDataStoreFactory.createNewDataStore(params);
    }

    protected void saveToMIF(File FileName, String charset) throws IOException, FactoryException {
        if (this.parcelCollection.size() != 0) {
            File RenameParcelFile = new File(FileName.getPath().replaceAll(
                    ".mif", "")
                    + "_Parcel" + ".mif");
            FileName.renameTo(RenameParcelFile);

            ReferencedEnvelope bounds = this.parcelCollection.getBounds();

            MIFDataStore polygonDataStore = createMIFDataStore(RenameParcelFile, bounds);

            SimpleFeatureType optType = createOptimizedType(parcelCollection, PARCEL_TYPE, 255);
            polygonDataStore.createSchema(optType);
            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
            String ParcelTypeName = polygonDataStore.getTypeNames()[0];
            SimpleFeatureSource parcelFeatureSource = polygonDataStore.getFeatureSource(ParcelTypeName);

            if (parcelFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) parcelFeatureSource, parcelCollection);
            } else {
                logger.warn(ParcelTypeName + " не поддерживает чтение/запись");
            }
            logger.info("Завершение конвертации участков. " + RenameParcelFile.getAbsolutePath());
        }

        if (this.quartalCollection.size() != 0) {
            File RenameQuartalFile = new File(FileName.getPath().replaceAll(
                    ".mif", "") + "_Qartal" + ".mif");
            FileName.renameTo(RenameQuartalFile);
            ReferencedEnvelope bounds = this.quartalCollection.getBounds();

            MIFDataStore polygonDataStore = createMIFDataStore(RenameQuartalFile, bounds);

            SimpleFeatureType optType = createOptimizedType(quartalCollection, QUARTAL_TYPE, 255);
            polygonDataStore.createSchema(optType);
            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
            String QuartalTypeName = polygonDataStore.getTypeNames()[0];
            SimpleFeatureSource quartalFeatureSource = polygonDataStore.getFeatureSource(QuartalTypeName);

            if (quartalFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) quartalFeatureSource, quartalCollection);
            } else {
                logger.warn(QuartalTypeName + " не поддерживает чтение/запись");
            }
            logger.info("Завершение конвертации квартала. " + RenameQuartalFile.getAbsolutePath());
        }
        if (this.zoneCollection.size() != 0) {
            File RenameZoneFile = new File(FileName.getPath().replaceAll(
                    ".mif", "") + "_Zones" + ".mif");
            FileName.renameTo(RenameZoneFile);

            ReferencedEnvelope bounds = this.zoneCollection.getBounds();

            MIFDataStore polygonDataStore = createMIFDataStore(RenameZoneFile, bounds);

            SimpleFeatureType optType = createOptimizedType(zoneCollection, ZONE_TYPE, 255);
            polygonDataStore.createSchema(optType);
            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
            String ZoneTypeName = polygonDataStore.getTypeNames()[0];
            SimpleFeatureSource zoneFeatureSource = polygonDataStore.getFeatureSource(ZoneTypeName);

            if (zoneFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) zoneFeatureSource, zoneCollection);
            } else {
                logger.warn(ZoneTypeName + " не поддерживает чтение/запись");
            }

            logger.info("Завершение конвертации зон. " + RenameZoneFile.getAbsolutePath());
        }
        if (this.zoneCollection.size() != 0) {
            File RenameBoundFile = new File(FileName.getPath().replaceAll(
                    ".mif", "") + "_Bounds" + ".mif");
            FileName.renameTo(RenameBoundFile);

            ReferencedEnvelope bounds = this.boundCollection.getBounds();

            MIFDataStore polygonDataStore = createMIFDataStore(RenameBoundFile, bounds);

            SimpleFeatureType optType = createOptimizedType(boundCollection, BOUND_TYPE, 255);
            polygonDataStore.createSchema(optType);
            // polygonDataStore.forceSchemaCRS(CRS.decode("EPSG:28415"));
            String BoundTypeName = polygonDataStore.getTypeNames()[0];
            SimpleFeatureSource boundFeatureSource = polygonDataStore.getFeatureSource(BoundTypeName);

            if (boundFeatureSource instanceof SimpleFeatureStore) {
                saveData((SimpleFeatureStore) boundFeatureSource, boundCollection);
            } else {
                logger.warn(BoundTypeName + " не поддерживает чтение/запись");
            }

            logger.info("Завершение конвертации границ. " + RenameBoundFile.getAbsolutePath());
        }
    }

    private SimpleFeatureType createOptimizedType(SimpleFeatureCollection Collection, SimpleFeatureType type, int maxLen) {
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setCRS(type.getCoordinateReferenceSystem());
        builder.setName(type.getName());
        builder.add("Geometry", type.getDescriptor(0).getType().getBinding());

        int count = type.getAttributeCount();
        for (int i = 1; i < count; i++) {
            int maxLength = calcMaxLength(i, Collection.features());
            builder.length(Math.min(maxLen, maxLength)).add(type.getDescriptor(i).getLocalName(), String.class);
        }
        try {
            return builder.buildFeatureType();
        } catch (Exception ex) {
            logger.error("Не удалость создать пространственный объект. ", ex);
            return null;
        }
    }

    private int calcMaxLength(int i, SimpleFeatureIterator iterator) {
        int maxLength = 1;
        try {
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                Object o = feature.getAttribute(i);
                if (o instanceof String) {
                    String s = (String)o;
                    if (maxLength < s.length())
                        maxLength = s.length();
//                    if (maxLength == 0)
//                    	maxLength = 1;
                }
            }
        } finally {
            iterator.close();
        }
        return maxLength;
    }

    protected void saveToPostGIS() throws IOException, FactoryException {
        if (this.parcelCollection.size() == 0) {
            logger.info("Нет данных для конвертации.");
            return;
        }

        Map<String,Object> params = new HashMap<String,Object>();
        params.put( "dbtype", "postgis");
        params.put( "host", "localhost");
        params.put( "port", 5432);
        params.put( "schema", "public");
        params.put( "database", "database");
        params.put( "user", "postgres");
        params.put( "passwd", "postgres");

        DataStore dataStore= DataStoreFinder.getDataStore(params);
        dataStore.createSchema(this.PARCEL_TYPE);
        String ParcelTypeName = dataStore.getTypeNames()[0];
        SimpleFeatureSource parcelFeatureSource = dataStore
                .getFeatureSource(ParcelTypeName);

        if (parcelFeatureSource instanceof SimpleFeatureStore) {
            saveData((SimpleFeatureStore) parcelFeatureSource, parcelCollection);
        } else {
            logger.warn(ParcelTypeName + " не поддерживает чтение/запись");
        }

        logger.info("Завершение конвертации. ");
    }

    private void saveData(SimpleFeatureStore FeatureStore, SimpleFeatureCollection Collection) throws IOException {
        Transaction transaction = new DefaultTransaction("create");
        FeatureStore.setTransaction(transaction);
        try {
            FeatureStore.addFeatures(Collection);
            transaction.commit();
            logger.info("Успешное сохранение." );

        } catch (Exception ex) {
            logger.error(
                "Ошибка при добавлении в shapefile.\nОткат изменений в shapefile, но файл возможно создан.",
                    ex);
            transaction.rollback();

        } finally {
            transaction.close();
            logger.info("Конец обработки.");
        }
    }

}//End of the class LandProcessor
