package org.tomskgislab.landprocessor;

//import javafx.fxml.Initializable;
//import javafx.fxml.FXML;
//import javafx.scene.control.CheckMenuItem;
//import javafx.scene.control.MenuItem;
//import javafx.scene.control.TextArea;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.tomskgislab.cadastre.xml.ExtFileFilter;

import java.util.ResourceBundle;
import java.net.URL;

import javax.swing.*;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.prefs.Preferences;

public class LandProcessorController /*implements Initializable*/ {
//	@FXML
//	private TextArea textAreaLog;
//	@FXML
//	private MenuItem miOpenXML;
//	@FXML
//	private MenuItem miSaveShape;
//	@FXML
//	private CheckMenuItem ckmiCP1251;
//	@FXML
//	private CheckMenuItem ckmiUTF8;
//	@FXML
//	private MenuItem miSaveMIDMIF;
//	@FXML
//	private MenuItem miHelp;
//	
//	private LandProcessor processor;
//	private static final long serialVersionUID = 1L;
//	private String workFilePath = "";
//	private File InitialDirectory = null;
//	private static Logger logger = LogManager.getLogger(LandProcessorController.class);
//
//	public LandProcessorController () {
//		readPrefs();
//	}
//	
//	private void disableSaveMenu() {
//		miSaveShape.setDisable(true);
//		miSaveMIDMIF.setDisable(true);
////		savePostGISMenuItem.setEnabled(false);
//	}
//
//	private void enableSaveMenu() {
//		miSaveShape.setDisable(false);
//		miSaveMIDMIF.setDisable(false);
////		savePostGISMenuItem.setEnabled(true);
//	}
//	
//	private void readPrefs() {
//		Preferences prefs = Preferences.userNodeForPackage(LandProcessorController.class);
//		String lastdir = prefs.get("lastdir", null);
//		if (lastdir != null) {
//			for (File lastfile = new File(lastdir); lastfile.getParentFile() != null; lastfile = lastfile
//					.getParentFile())
//				if (lastfile.exists()) {
//					InitialDirectory = lastfile;
//					break;
//				}
//		}
//	}
//	
//	private void saveLastDir() {
//		Preferences prefs = Preferences.userNodeForPackage(MainForm.class);
//		prefs.put("lastdir", InitialDirectory.getAbsolutePath());
//	}
//	
//	private String getCharset() {
//		if (ckmiCP1251.isSelected())
//			return ckmiCP1251.getText();
//		return ckmiUTF8.getText();
//	}
//
//
//    @Override
//	public void initialize(URL url, ResourceBundle resourceBundle) {
//        TextAreaHandlerFX.setTextArea(this.textAreaLog);
//        Logger.getRootLogger().addAppender(new TextAreaHandlerFX());
//        processor = new LandProcessor();
//		textAreaLog.setText("Начало работы LandProcessor...JavaFX\nРазработка:\n" +
//				"Филиппов Владислав filippov70@gmail.com\n"
//				+ "Першин Денис dyp@perchine.com\n");
//		textAreaLog.end();
//	}
//
//    @FXML
//    protected void initialize() {
//
//    }
//
//	@FXML
//	public void HandleMiOpenXML() {
//		try {
//			if (processor != null)
//				processor = null;
//
//			ExtFileFilter filter = new ExtFileFilter("xml", "Кадастровый XML");
//			File file;
//			File[] files = null;
//			JFileDataStoreChooser dialog = new JFileDataStoreChooser("xml");
//			dialog.setFileFilter(filter);
//			dialog.setMultiSelectionEnabled(true);
//			dialog.setAcceptAllFileFilterUsed(false);
//
//			if (InitialDirectory != null) {
//				if (InitialDirectory.isDirectory()) {
//					dialog.setCurrentDirectory(InitialDirectory);
//				} else {
//					dialog.setCurrentDirectory(InitialDirectory.getParentFile());
//				}
//			}
//			if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
//				// file = dialog.getSelectedFile();
//				files = dialog.getSelectedFiles();
//			}
//			if (files == null) {
//				return;
//			}
//
//			if (files.length == 1) {
//				file = files[0];
//				InitialDirectory = file.getParentFile();
//				workFilePath = file.getAbsolutePath().substring(0,
//						file.getAbsolutePath().length() - 4);
//				logger.info("Открыт файл: " + file.getPath() + "\n");
//				processor = new LandProcessor();
//				processor.ProcessXMLFile(file);
//
//			} else {
//				logger.info("-=НАЧАЛО ПАКЕТНОЙ ОБРАБОТКИ=-");
//				for (File file1 : files) {
//					file = file1;
//					InitialDirectory = file.getParentFile();
//					logger.info("Открыт файл: " + file.getPath() + "\n");
//					workFilePath = file.getAbsolutePath().substring(0,
//							file.getAbsolutePath().length() - 4);
//
//					processor = new LandProcessor();
//					processor.ProcessXMLFile(file);
//					disableSaveMenu();
//					try {
//						processor.saveToShape(new File(workFilePath),
//								getCharset());
//					} catch (MalformedURLException murlex) {
//						logger.error("", murlex);
//					} catch (IOException ioex) {
//						logger.error("", ioex);
//					}
//					processor = null;
//				}
//				logger.info("-=КОНЕЦ ПАКЕТНОЙ ОБРАБОТКИ=-");
//			}
////            if (!processor.IsError()) {
//			    saveLastDir();
//			    enableSaveMenu();
////            }
//		} catch (Exception ex) {
//			logger.error(ex);
//		}
//	}
//
//    @FXML
//    public void HandleMiSaveShape() {
//        JFileDataStoreChooser chooser = new JFileDataStoreChooser("shp");
//        chooser.setDialogTitle("Save shapefiles");
//        chooser.setSelectedFile(new File(this.workFilePath + ".shp"));
//        int returnVal = chooser.showSaveDialog(null);
//
//        if (returnVal != JFileDataStoreChooser.APPROVE_OPTION) {
//            return;
//        }
//        try {
//            processor.saveToShape(chooser.getSelectedFile(), getCharset());
//        } catch (MalformedURLException mURLEx) {
//            logger.error("", mURLEx);
//        } catch (NoSuchAuthorityCodeException nsacex) {
//            logger.error("", nsacex);
//        } catch (IOException ioex) {
//            logger.error("", ioex);
//        } catch (FactoryException fex) {
//            logger.error("", fex);
//        }
//
//        processor = null;
//        //disableSaveMenu();
//    }
//    
//    @FXML
//    public void HandleMiSaveMIDMIF() {
//    	JFileDataStoreChooser chooser = new JFileDataStoreChooser("mif");
//		chooser.setDialogTitle("Save MIF");
//		chooser.setSelectedFile(new File(this.workFilePath + ".mif"));
//		int returnVal = chooser.showSaveDialog(null);
//
//		if (returnVal != JFileDataStoreChooser.APPROVE_OPTION) {
//			return;
//		}
//		try {
//			processor.saveToMIF(chooser.getSelectedFile(), getCharset());
//		} catch (MalformedURLException mURLEx) {
//			logger.error("", mURLEx);
//		} catch (NoSuchAuthorityCodeException nsacex) {
//			logger.error("", nsacex);
//		} catch (IOException ioex) {
//			logger.error("", ioex);
//		} catch (FactoryException fex) {
//			logger.error("", fex);
//		}
//
//		processor = null;
//		//disableSaveMenu();
//    }
//    
//    @FXML
//    public void HandleMiHelp() {
//    	Desktop desktop;
//		if (Desktop.isDesktopSupported()) {
//			desktop = Desktop.getDesktop();
//			if (desktop.isSupported(Desktop.Action.BROWSE)) {
//				// launch browser
//				URI uri;
//				try {
//					uri = new URI(
//							"https://sites.google.com/site/landprocessorproject/");
//					desktop.browse(uri);
//				} catch (IOException ioex) {
//					logger.error("", ioex);
//				} catch (URISyntaxException usex) {
//					logger.error("", usex);
//				}
//			}
//		}
//    }
//    
//    @FXML
//    public void HandleMiCP1251(){
//    	ckmiCP1251.setSelected(true);
//    	ckmiUTF8.setSelected(false);	
//    	
//    }
//    
//    @FXML
//    public void HandleMiUTF8(){
//    	ckmiCP1251.setSelected(false);
//    	ckmiUTF8.setSelected(true);
//    }
//    
//	public TextArea getTextAreaLog() {
//		return textAreaLog;
//	}

}
