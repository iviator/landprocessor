package org.tomskgislab.landprocessor.constant;

public class LandProcessorConstants {
	// Дефолтная система координат
    public static final String EPSG_UTM_45N = "PROJCS[\"WGS 84 / UTM zone 45N\", \n"
            + "  GEOGCS[\"WGS 84\", \n"
            + "    DATUM[\"World Geodetic System 1984\", \n"
            + "      SPHEROID[\"WGS 84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7030\"]], \n"
            + "      AUTHORITY[\"EPSG\",\"6326\"]], \n"
            + "    PRIMEM[\"Greenwich\", 0.0, AUTHORITY[\"EPSG\",\"8901\"]], \n"
            + "    UNIT[\"degree\", 0.017453292519943295], \n"
            + "    AXIS[\"Geodetic longitude\", EAST], \n"
            + "    AXIS[\"Geodetic latitude\", NORTH], \n"
            + "    AUTHORITY[\"EPSG\",\"4326\"]], \n"
            + "  PROJECTION[\"Transverse_Mercator\", AUTHORITY[\"EPSG\",\"9807\"]], \n"
            + "  PARAMETER[\"central_meridian\", 87.0], \n"
            + "  PARAMETER[\"latitude_of_origin\", 0.0], \n  PARAMETER[\"scale_factor\", 0.9996], \n"
            + "  PARAMETER[\"false_easting\", 500000.0], \n"
            + "  PARAMETER[\"false_northing\", 0.0], \n"
            + "  UNIT[\"m\", 1.0], \n"
            + "  AXIS[\"Easting\", EAST], \n"
            + "  AXIS[\"Northing\", NORTH], \n"
            + "  AUTHORITY[\"EPSG\",\"32645\"]]";
    
    // Путь к словарям используемым в схемах
    // http://skipy-ru.livejournal.com/5343.html
    // http://spec-zone.ru/RU/Java/Docs/7/technotes/guides/lang/resources.html
    //public static final String PATH_TO_XSD = "shema/";
    public static final String PATH_TO_XSD = "/shema/";
    
    public static final String KV = "Region_Cadastr_Vidimus";
    public static final String KV1 = "Region_Cadastr_Vidimus_KV";
    public static final String KP = "Region_Cadastr_Vidimus_KP";
    public static final String KPT = "Region_Cadastr";
    // 
    final String KVZU = "Region_Cadastr_Vidimus_KV";
    
 // Словари
    public static final String dArea = "dArea";
    public static final String dUnit = "dUnit";
    public static final String dCategories = "dCategories";
    public static final String dUtilizations = "dUtilizations";
    public static final String dStates = "dStates";
    public static final String dRights = "dRights";
    public static final String dRealty = "dRealty";
    public static final String dEncumbrances = "dEncumbrances";
    public static final String dRegions = "dRegionsRF";
    public static final String dAllDocuments = "dAllDocuments";
    public static final String dPermitUse = "dPermitUse";

    public static final String PARCEL_TYPE_01 = "Землепользование";
    public static final String PARCEL_TYPE_02 = "Единое землепользование";
    public static final String PARCEL_TYPE_03 = "Обособленный участок";
    public static final String PARCEL_TYPE_04 = "Условный участок";
    public static final String PARCEL_TYPE_05 = "Многоконтурный участок";
    public static final String PARCEL_TYPE_06 = "Значение отсутствует";
    
    public static final String NODATA = "--";
    
}//End of the class LandProcessorConstants
