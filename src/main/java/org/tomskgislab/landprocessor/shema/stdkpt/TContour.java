//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.10.15 at 04:02:51 PM NOVT 
//


package org.tomskgislab.landprocessor.shema.stdkpt;

import javax.xml.bind.annotation.*;


/**
 * Описание контура участка
 * 
 * <p>Java class for tContour complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tContour">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Entity_Spatial" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Number_PP" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;maxLength value="40"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tContour", propOrder = {
    "entitySpatial"
})
public class TContour {

    @XmlElement(name = "Entity_Spatial")
    protected EntitySpatial entitySpatial;
    @XmlAttribute(name = "Number_PP", required = true)
    protected String numberPP;

    /**
     * Gets the value of the entitySpatial property.
     * 
     * @return
     *     possible object is
     *     {@link EntitySpatial }
     *     
     */
    public EntitySpatial getEntitySpatial() {
        return entitySpatial;
    }

    /**
     * Sets the value of the entitySpatial property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitySpatial }
     *     
     */
    public void setEntitySpatial(EntitySpatial value) {
        this.entitySpatial = value;
    }

    /**
     * Gets the value of the numberPP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberPP() {
        return numberPP;
    }

    /**
     * Sets the value of the numberPP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberPP(String value) {
        this.numberPP = value;
    }

}
