//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.02.07 at 07:04:30 PM NOVT 
//


package org.tomskgislab.landprocessor.shema.stdkp;

import javax.xml.bind.annotation.*;


/**
 * Кадастровый квартал
 * 
 * <p>Java class for tCadastral_Block complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tCadastral_Block">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Location">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Code_OKATO">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="11"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{}Parcels" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CadastralNumber" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;maxLength value="40"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tCadastral_Block", propOrder = {
    "location",
    "parcels"
})
public class TCadastralBlock {

    @XmlElement(name = "Location", required = true)
    protected TCadastralBlock.Location location;
    @XmlElement(name = "Parcels")
    protected Parcels parcels;
    @XmlAttribute(name = "CadastralNumber", required = true)
    protected String cadastralNumber;

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link TCadastralBlock.Location }
     *     
     */
    public TCadastralBlock.Location getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link TCadastralBlock.Location }
     *     
     */
    public void setLocation(TCadastralBlock.Location value) {
        this.location = value;
    }

    /**
     * Gets the value of the parcels property.
     * 
     * @return
     *     possible object is
     *     {@link Parcels }
     *     
     */
    public Parcels getParcels() {
        return parcels;
    }

    /**
     * Sets the value of the parcels property.
     * 
     * @param value
     *     allowed object is
     *     {@link Parcels }
     *     
     */
    public void setParcels(Parcels value) {
        this.parcels = value;
    }

    /**
     * Gets the value of the cadastralNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCadastralNumber() {
        return cadastralNumber;
    }

    /**
     * Sets the value of the cadastralNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCadastralNumber(String value) {
        this.cadastralNumber = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Code_OKATO">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="11"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codeOKATO"
    })
    public static class Location {

        @XmlElement(name = "Code_OKATO", required = true)
        protected String codeOKATO;

        /**
         * Gets the value of the codeOKATO property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeOKATO() {
            return codeOKATO;
        }

        /**
         * Sets the value of the codeOKATO property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeOKATO(String value) {
            this.codeOKATO = value;
        }

    }

}
