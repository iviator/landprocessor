package org.tomskgislab.landprocessor.shema.share;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.tomskgislab.landprocessor.constant.LandProcessorConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для загрузки данных из справочников. Справочников нужно взять по
 * максимуму в схемах. Это общий для всех класс.
 * 
 * @author Filippov Vladislav
 * @since 1.0
 */
public class CadastreDictionaryLoader {

	private static Logger logger = LogManager
			.getLogger(CadastreDictionaryLoader.class);

	private List<CadastreDictionary> dictionaries;
	
	// Происходит разбор всех файлов в директории
	public CadastreDictionaryLoader(String path) {
		dictionaries = new ArrayList<CadastreDictionary>();
		File pathToDicts = new File(path);
		logger.info(pathToDicts);
		File files[] = pathToDicts.listFiles();
		if (files == null)
			return;
		for (File file : files) {
			DocumentBuilderFactory domFactory = DocumentBuilderFactory
					.newInstance();
			domFactory.setNamespaceAware(true); // never forget this!
			if (!file.isDirectory()) {
				try {
					DocumentBuilder builder = domFactory.newDocumentBuilder();
					Document doc = builder.parse(file);
					String DocName = file.getName()
							.substring(0, file.getName()
									.lastIndexOf('.'));
					parseDictionary(doc, DocName);
				} catch (Exception ex) {
					logger.error(ex);
				}
			}
		}
	}

	private void parseDictionary(Document doc, String DocName) {
		try {
			logger.info("Загрузка словаря " + DocName);
			NodeList nodes = doc.getElementsByTagName("xs:enumeration");
			dictionaries.add(new CadastreDictionary(DocName));
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				//logger.info("Узел " + n.getLocalName() + " " + n.getAttributes().item(0).toString());
				String Value = n.getAttributes().item(0).getTextContent();
				String Descr = n.getChildNodes().item(1).getChildNodes().item(1).getTextContent(); 
				dictionaries.get(dictionaries.size()-1).AddItem(Value, Descr);
			}
		} catch (Exception ex) {
			logger.error("Словарь " + DocName + " удалён; " + ex);
			dictionaries.remove(dictionaries.size()-1);
		}

	}

	public List<CadastreDictionary> getDictionaries() {
		return dictionaries;
	}

	public String getValueFromDictionary(String DictName, String Key) {
		for (CadastreDictionary dictionary : dictionaries)
			if (dictionary.getName().equals(DictName))
				return dictionary.getValue(Key);
		return LandProcessorConstants.NODATA;
	}
	
	public String getKeyFromDictionary(String DictName, String Value) {
		for (CadastreDictionary dictionary : dictionaries)
			if (dictionary.getName().equals(DictName))
				return dictionary.getKey(Value);
		return LandProcessorConstants.NODATA;
	}
}

