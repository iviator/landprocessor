package org.tomskgislab.landprocessor.shema.share;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Класс для работы со справочниками из XSD
 *  @author Филиппов Владислав
 *	@since 1.0
 */
public class CadastreDictionary {
	private static Logger logger = LogManager
			.getLogger(CadastreDictionary.class);
	private String name;
	private Map<String, String> item;
	
	public CadastreDictionary(String Name) {
		item = new HashMap<String, String>();
		name = Name;
	}
	
	public String getValue(String Key) {
		if (item.containsKey(Key)) {
			return item.get(Key);
		}
		else{
			return "";
		}
	}
	
	public String getKey(String Value) {
		Set<Entry<String, String>> entries = item.entrySet();
		Iterator<Entry<String, String>> iterator = entries.iterator();
		while (iterator.hasNext()) {
		   Entry<String, String> entry = iterator.next();
		   if (entry.getKey().equals(Value)) {
			   return (String)entry.getValue();
		   }
		}
		return "";
	}
	
	public void AddItem(String Key, String Value) {
		item.put(Key, Value);
	}

	public String getName() {
		return name;
	}
}
