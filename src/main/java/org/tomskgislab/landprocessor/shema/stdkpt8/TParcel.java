//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.05 at 09:59:04 AM NOVT 
//


package org.tomskgislab.landprocessor.shema.stdkpt8;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Описание земельного участка
 * 
 * <p>Java class for tParcel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tParcel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Area">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{}tArea">
 *                 &lt;sequence>
 *                   &lt;element name="Innccuracy" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                         &lt;totalDigits value="20"/>
 *                         &lt;fractionDigits value="2"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Location" type="{}tLocation"/>
 *         &lt;element name="Category" type="{}tCategory"/>
 *         &lt;element name="Utilization" type="{}tUtilization"/>
 *         &lt;element name="Rights" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Right" type="{}tRight" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SubParcels" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubParcel" type="{}tSubParcel" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Unified_Land_Unit" type="{}tUunified_land_unit" minOccurs="0"/>
 *         &lt;element name="Contours" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Contour" type="{}tContour" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Encumbrances" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Encumbrance" type="{}tEncumbrance" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CadastralCost" type="{}tCadastralCost" minOccurs="0"/>
 *         &lt;element ref="{}Entity_Spatial" minOccurs="0"/>
 *         &lt;element ref="{}Coord_System" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CadastralNumber" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;maxLength value="40"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="Name" use="required" type="{}dParcels" />
 *       &lt;attribute name="State" use="required" type="{}dStates" />
 *       &lt;attribute name="DateCreated" type="{http://www.w3.org/2001/XMLSchema}date" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tParcel", propOrder = {
    "area",
    "location",
    "category",
    "utilization",
    "rights",
    "subParcels",
    "unifiedLandUnit",
    "contours",
    "encumbrances",
    "cadastralCost",
    "entitySpatial",
    "coordSystem"
})
public class TParcel {

    @XmlElement(name = "Area", required = true)
    protected TParcel.Area area;
    @XmlElement(name = "Location", required = true)
    protected TLocation location;
    @XmlElement(name = "Category", required = true)
    protected TCategory category;
    @XmlElement(name = "Utilization", required = true)
    protected TUtilization utilization;
    @XmlElement(name = "Rights")
    protected TParcel.Rights rights;
    @XmlElement(name = "SubParcels")
    protected TParcel.SubParcels subParcels;
    @XmlElement(name = "Unified_Land_Unit")
    protected TUunifiedLandUnit unifiedLandUnit;
    @XmlElement(name = "Contours")
    protected TParcel.Contours contours;
    @XmlElement(name = "Encumbrances")
    protected TParcel.Encumbrances encumbrances;
    @XmlElement(name = "CadastralCost")
    protected TCadastralCost cadastralCost;
    @XmlElement(name = "Entity_Spatial")
    protected EntitySpatial entitySpatial;
    @XmlElement(name = "Coord_System")
    protected CoordSystem coordSystem;
    @XmlAttribute(name = "CadastralNumber", required = true)
    protected String cadastralNumber;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "State", required = true)
    protected String state;
    @XmlAttribute(name = "DateCreated")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateCreated;

    /**
     * Gets the value of the area property.
     * 
     * @return
     *     possible object is
     *     {@link TParcel.Area }
     *     
     */
    public TParcel.Area getArea() {
        return area;
    }

    /**
     * Sets the value of the area property.
     * 
     * @param value
     *     allowed object is
     *     {@link TParcel.Area }
     *     
     */
    public void setArea(TParcel.Area value) {
        this.area = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link TLocation }
     *     
     */
    public TLocation getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link TLocation }
     *     
     */
    public void setLocation(TLocation value) {
        this.location = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link TCategory }
     *     
     */
    public TCategory getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link TCategory }
     *     
     */
    public void setCategory(TCategory value) {
        this.category = value;
    }

    /**
     * Gets the value of the utilization property.
     * 
     * @return
     *     possible object is
     *     {@link TUtilization }
     *     
     */
    public TUtilization getUtilization() {
        return utilization;
    }

    /**
     * Sets the value of the utilization property.
     * 
     * @param value
     *     allowed object is
     *     {@link TUtilization }
     *     
     */
    public void setUtilization(TUtilization value) {
        this.utilization = value;
    }

    /**
     * Gets the value of the rights property.
     * 
     * @return
     *     possible object is
     *     {@link TParcel.Rights }
     *     
     */
    public TParcel.Rights getRights() {
        return rights;
    }

    /**
     * Sets the value of the rights property.
     * 
     * @param value
     *     allowed object is
     *     {@link TParcel.Rights }
     *     
     */
    public void setRights(TParcel.Rights value) {
        this.rights = value;
    }

    /**
     * Gets the value of the subParcels property.
     * 
     * @return
     *     possible object is
     *     {@link TParcel.SubParcels }
     *     
     */
    public TParcel.SubParcels getSubParcels() {
        return subParcels;
    }

    /**
     * Sets the value of the subParcels property.
     * 
     * @param value
     *     allowed object is
     *     {@link TParcel.SubParcels }
     *     
     */
    public void setSubParcels(TParcel.SubParcels value) {
        this.subParcels = value;
    }

    /**
     * Gets the value of the unifiedLandUnit property.
     * 
     * @return
     *     possible object is
     *     {@link TUunifiedLandUnit }
     *     
     */
    public TUunifiedLandUnit getUnifiedLandUnit() {
        return unifiedLandUnit;
    }

    /**
     * Sets the value of the unifiedLandUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link TUunifiedLandUnit }
     *     
     */
    public void setUnifiedLandUnit(TUunifiedLandUnit value) {
        this.unifiedLandUnit = value;
    }

    /**
     * Gets the value of the contours property.
     * 
     * @return
     *     possible object is
     *     {@link TParcel.Contours }
     *     
     */
    public TParcel.Contours getContours() {
        return contours;
    }

    /**
     * Sets the value of the contours property.
     * 
     * @param value
     *     allowed object is
     *     {@link TParcel.Contours }
     *     
     */
    public void setContours(TParcel.Contours value) {
        this.contours = value;
    }

    /**
     * Gets the value of the encumbrances property.
     * 
     * @return
     *     possible object is
     *     {@link TParcel.Encumbrances }
     *     
     */
    public TParcel.Encumbrances getEncumbrances() {
        return encumbrances;
    }

    /**
     * Sets the value of the encumbrances property.
     * 
     * @param value
     *     allowed object is
     *     {@link TParcel.Encumbrances }
     *     
     */
    public void setEncumbrances(TParcel.Encumbrances value) {
        this.encumbrances = value;
    }

    /**
     * Gets the value of the cadastralCost property.
     * 
     * @return
     *     possible object is
     *     {@link TCadastralCost }
     *     
     */
    public TCadastralCost getCadastralCost() {
        return cadastralCost;
    }

    /**
     * Sets the value of the cadastralCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link TCadastralCost }
     *     
     */
    public void setCadastralCost(TCadastralCost value) {
        this.cadastralCost = value;
    }

    /**
     * Gets the value of the entitySpatial property.
     * 
     * @return
     *     possible object is
     *     {@link EntitySpatial }
     *     
     */
    public EntitySpatial getEntitySpatial() {
        return entitySpatial;
    }

    /**
     * Sets the value of the entitySpatial property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitySpatial }
     *     
     */
    public void setEntitySpatial(EntitySpatial value) {
        this.entitySpatial = value;
    }

    /**
     * Gets the value of the coordSystem property.
     * 
     * @return
     *     possible object is
     *     {@link CoordSystem }
     *     
     */
    public CoordSystem getCoordSystem() {
        return coordSystem;
    }

    /**
     * Sets the value of the coordSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordSystem }
     *     
     */
    public void setCoordSystem(CoordSystem value) {
        this.coordSystem = value;
    }

    /**
     * Gets the value of the cadastralNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCadastralNumber() {
        return cadastralNumber;
    }

    /**
     * Sets the value of the cadastralNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCadastralNumber(String value) {
        this.cadastralNumber = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the dateCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateCreated() {
        return dateCreated;
    }

    /**
     * Sets the value of the dateCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateCreated(XMLGregorianCalendar value) {
        this.dateCreated = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{}tArea">
     *       &lt;sequence>
     *         &lt;element name="Innccuracy" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *               &lt;totalDigits value="20"/>
     *               &lt;fractionDigits value="2"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "innccuracy"
    })
    public static class Area
        extends TArea
    {

        @XmlElement(name = "Innccuracy")
        protected BigDecimal innccuracy;

        /**
         * Gets the value of the innccuracy property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getInnccuracy() {
            return innccuracy;
        }

        /**
         * Sets the value of the innccuracy property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setInnccuracy(BigDecimal value) {
            this.innccuracy = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Contour" type="{}tContour" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contour"
    })
    public static class Contours {

        @XmlElement(name = "Contour", required = true)
        protected List<TContour> contour;

        /**
         * Gets the value of the contour property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contour property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContour().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TContour }
         * 
         * 
         */
        public List<TContour> getContour() {
            if (contour == null) {
                contour = new ArrayList<TContour>();
            }
            return this.contour;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Encumbrance" type="{}tEncumbrance" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "encumbrance"
    })
    public static class Encumbrances {

        @XmlElement(name = "Encumbrance", required = true)
        protected List<TEncumbrance> encumbrance;

        /**
         * Gets the value of the encumbrance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the encumbrance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEncumbrance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TEncumbrance }
         * 
         * 
         */
        public List<TEncumbrance> getEncumbrance() {
            if (encumbrance == null) {
                encumbrance = new ArrayList<TEncumbrance>();
            }
            return this.encumbrance;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Right" type="{}tRight" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "right"
    })
    public static class Rights {

        @XmlElement(name = "Right", required = true)
        protected List<TRight> right;

        /**
         * Gets the value of the right property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the right property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRight().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TRight }
         * 
         * 
         */
        public List<TRight> getRight() {
            if (right == null) {
                right = new ArrayList<TRight>();
            }
            return this.right;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubParcel" type="{}tSubParcel" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subParcel"
    })
    public static class SubParcels {

        @XmlElement(name = "SubParcel", required = true)
        protected List<TSubParcel> subParcel;

        /**
         * Gets the value of the subParcel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subParcel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubParcel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TSubParcel }
         * 
         * 
         */
        public List<TSubParcel> getSubParcel() {
            if (subParcel == null) {
                subParcel = new ArrayList<TSubParcel>();
            }
            return this.subParcel;
        }

    }

}
