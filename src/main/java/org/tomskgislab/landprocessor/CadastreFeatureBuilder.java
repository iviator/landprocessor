/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */
package org.tomskgislab.landprocessor;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * Cоздатель кадастровых объектов.
 * Содержит описание пространственных объектов.
 * @author Филиппов Владислав
 */
public class CadastreFeatureBuilder {

private static Logger logger = LogManager.getLogger( CadastreFeatureBuilder.class );

    public enum CadastreFeatureTypes {
        CharacterPoint,     // Характерная точка
        Parcel,             // Участок
        Quartal,            // Кадастровый квартал
        Zones,          	// Территориальные зоны и Зоны с особыми условиями
        Bounds          	// Границы муниципальных образований и поселений
    }
    
    /**
     * Создатель типов пространственных объектов.
     * @param crs Система координат
     * @param FeatureType тип создаваемого объекта (enum)
     * @return Созданая фича 
     */
    public static SimpleFeatureType createFeatureType(
            CoordinateReferenceSystem crs, CadastreFeatureTypes FeatureType) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setCRS(crs);
        switch (FeatureType)
        {
        	// По просьбам трудящихся точки
            case CharacterPoint :
                builder.setName("CharacterPoint");
                builder.add("Geometry", Point.class);
                builder.length(150).add("Name", String.class);
                builder.length(20).add("SuNumb", String.class);
                builder.length(20).add("OrdNumb", String.class);
                builder.length(20).add("NumGeoPt", String.class);
                builder.length(5).add("Delta", String.class);
                builder.length(150).add("PointHold", String.class);
                break;
            case Parcel :
                builder.setName("Polygon");
                builder.add("Geometry", MultiPolygon.class);
                builder.length(150).add("Name", String.class);
                builder.length(25).add("Cadnumber", String.class);
                builder.length(255).add("UtilizD", String.class);
                builder.length(255).add("UtilizT", String.class);
                // В схеме это "Name", хотя обозначает тип
                builder.length(100).add("Type", String.class);
                builder.length(100).add("State", String.class);
                
                // Могут быть 2 права (Собственность у РФ и ПНВ у какой-нибудь госгонторы)
                builder.length(255).add("Right1", String.class);
                builder.length(255).add("Right2", String.class);
            
                builder.length(255).add("Category", String.class);
                //
                builder.length(255).add("Area", String.class);
                builder.length(255).add("AreaUnit", String.class);
                
                // Поля для КВЗУ
                builder.length(255).add("Owner", String.class);
                builder.length(255).add("Owner1", String.class);
                
                // Поля для чЗУ
                // ObjectEntry
                builder.length(255).add("OENumber", String.class);
                builder.length(255).add("OEType", String.class);
                //Encumbrance
                builder.length(255).add("EName", String.class);
                builder.length(255).add("EType", String.class);
                builder.length(255).add("EOwner", String.class);
                //Location
                builder.length(255).add("Region", String.class);
                builder.length(255).add("Parish", String.class);
                builder.length(255).add("City", String.class);
                builder.length(255).add("District", String.class);
                builder.length(255).add("Village", String.class);
                builder.length(255).add("Locality", String.class);
                builder.length(255).add("Street", String.class);
                builder.length(255).add("Level1", String.class);
                builder.length(255).add("Level2", String.class);
                builder.length(255).add("Level3", String.class);
                builder.length(255).add("Flat", String.class);

                builder.length(255).add("Other", String.class);
                builder.length(255).add("Note", String.class);
                builder.length(255).add("Note2", String.class);
                break;
            case Quartal :
                builder.setName("Quartal");
                builder.add("Geometry", MultiPolygon.class);
                builder.length(255).add("Name", String.class);
                builder.length(255).add("Area", String.class);
                builder.length(255).add("Note", String.class);
                builder.length(255).add("CoordSys", String.class);
                builder.length(255).add("Cadnumber", String.class);
                break;
            
            case Zones :
            	builder.setName("Zones");
            	builder.add("Geometry", MultiPolygon.class);
            	builder.length(150).add("Name", String.class);
            	builder.length(255).add("Number", String.class);
            	builder.length(255).add("DocCode", String.class);
            	builder.length(255).add("DocName", String.class);
            	builder.length(255).add("DocNumber", String.class);
            	builder.length(255).add("DocDate", String.class);
            	builder.length(255).add("DocIssue", String.class);
            	
            	builder.length(255).add("Restrict", String.class);
            	
            	builder.length(255).add("LandUse", String.class);
            	builder.length(255).add("TypePerm", String.class);
            	builder.length(255).add("PermUse", String.class);
                break;

            case Bounds :
                builder.setName("Bounds");
                builder.add("Geometry", MultiPolygon.class);
                builder.length(150).add("Name", String.class);
                builder.length(255).add("Number", String.class);
                builder.length(255).add("DocCode", String.class);
                builder.length(255).add("DocName", String.class);
                builder.length(255).add("DocNumber", String.class);
                builder.length(255).add("DocDate", String.class);
                builder.length(255).add("DocIssue", String.class);

                builder.length(255).add("Type", String.class);
                builder.length(255).add("Name", String.class);

                break;

            default:
               logger.warn("Нет обработчика для данного типа: " + FeatureType.toString());
               return  null;
        }
        

        // build the type
        try {
            return builder.buildFeatureType();
        }
        catch (Exception ex) {
            logger.error("Не удалость создать пространственный объект. ", ex);
            return null;
        }
        
    }    
}
