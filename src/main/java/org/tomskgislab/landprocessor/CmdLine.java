package org.tomskgislab.landprocessor;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Copyright Denis Pershin 2013
 */
public class CmdLine {
	private static Logger logger = LogManager.getLogger(CmdLine.class);

    public static void main(String [ ] args) {
        if (args.length < 4) {
            usage();
            return;
        }

        // todo Сделать для выбора GUI

        if (!"shp".equals(args[1]) && !"mif".equals(args[1])) {
            System.err.println("Invalid file type specified: " + args[1]);
            usage();
            return;
        }

        LandProcessor processor = new LandProcessor();
        try {
            processor.ProcessXMLFile(new File(args[2]));
            if ("shp".equals(args[0]))
                processor.saveToShape(new File(args[3]), "UTF8");
            else if ("mif".equals(args[0]))
                processor.saveToMIF(new File(args[3]), "UTF8");
        } catch (Exception x) {
            logger.error(x.getMessage());
            x.printStackTrace();
        }
    }

    private static void usage() {
        System.err.println("Usage: landprocessor {javafx|swing} {shp|mif} <input> <output>");
    }
}
