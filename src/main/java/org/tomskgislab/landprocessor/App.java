/*
 * LandProcessor - конвертер XML Росреестра в Shape-файлы
 * Copyright 2011 Филиппов Владислав, aka nukevlad, aka filippov70
 * filippov70@gmail.com Россия, Томск
 * 
 * Этот файл — часть LandProcessor.

   LandProcessor - свободная программа: вы можете перераспространять её и/или
   изменять её на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   LandProcessor распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <http://www.gnu.org/licenses/>.)
 */
package org.tomskgislab.landprocessor;

//import javafx.application.Application;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Scene;
//import javafx.scene.layout.AnchorPane;
//import javafx.stage.Stage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.factory.GeoTools;
import java.net.URL;

public class App /*extends Application*/ {

    private static final Logger logger = LogManager.getLogger( App.class );
    
    public static void main(String[] args) {
//        if (args.length < 1) {
//            return;
//        }
//        if (!"javafx".equals(args[0]) && !"swing".equals(args[0])) {
//
//        }
//        if ("javafx".equals(args[0])) {
//            launch(args);
//        }
//        else if ("swing".equals(args[0])) {
            MainForm frm = new MainForm();
            frm.show();
//        }
    }

//    @Override
//    public void start(Stage primaryStage) {
//        //Stage primaryStage1 = primaryStage;
//        //primaryStage1.setTitle("LandProcessor 4-SNAPSHOT GNU GPL. JavaFX. GeoTools " + GeoTools.getVersion());
//        try {
//
//            //URL urlView = new URL(App.class.getResource("/org/tomskgislab/landprocessor/LandProcessorView.fxml"));
//            //logger.info(App.class.getResource("/org/tomskgislab/landprocessor/LandProcessorView.fxml").toString());
//            FXMLLoader loader = new FXMLLoader(App.class.getResource("/LandProcessorView.fxml"));
//            logger.info("loading fxml...");
//            AnchorPane rootLayout = (AnchorPane)loader.load();
//            logger.info("loading fxml complit");
//            Scene scene = new Scene(rootLayout);
//            primaryStage.setTitle("LandProcessor 4.2 GNU GPL. JavaFX. GeoTools " + GeoTools.getVersion());
//            primaryStage.setScene(scene);
//            primaryStage.show();
//            logger.info("Application started");
//        } catch (Exception ex) {
//        	logger.error(ex);
//            System.exit(-1);
//        }
//    }
}