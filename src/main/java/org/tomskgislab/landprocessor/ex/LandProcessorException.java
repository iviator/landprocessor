package org.tomskgislab.landprocessor.ex;

public class LandProcessorException extends Exception{

    public LandProcessorException() {
    }

    public LandProcessorException(String string) {
        super(string);
    }

    public LandProcessorException(Throwable thrwbl) {
        super(thrwbl);
    }

    public LandProcessorException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }
    
}//End of the class LandProcessorException
