package org.tomskgislab.landprocessor;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

public class TextAreaHandler extends WriterAppender {

    static private JTextArea jTextArea = null;

	static public void setTextArea(JTextArea JTextAreaLog) {
		TextAreaHandler.jTextArea = JTextAreaLog;
	}


    public void doAppend(LoggingEvent arg0) {
        final String message = arg0.getMessage().toString() + "\n";
//		final Color color;
//		if (arg0.getLevel() == Level.ERROR) {
//			color = Color.RED;
//		} else {
//			color = Color.BLUE;
//		}

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // jTextArea.append(message + "\n");
                if (jTextArea != null) {
                    jTextArea.append(message);
                    jTextArea.setCaretPosition(jTextArea.getDocument().getLength());
                }
            }
        });
    }

}
